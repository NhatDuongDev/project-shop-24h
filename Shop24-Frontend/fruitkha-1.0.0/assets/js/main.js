(function($) {
	'use strict';

	$(document).ready(function($) {
		/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
		// biến chứa mảng các sản phẩm được chọn
		var gArrayProduct = null;
		var gChangeTypeSearch = 1;

		/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
		onPageLoading();

		$('#product-card').on('click', '.add-product-order-cart', function() {
			addProductOrderCart(this);
		});

		$('#add-product-order-cart').on('click', addSingleProductOrderCart);

		$('.search-area').on('change', '#select-type-search', changeSelectTypeSearch);
		$('.search-area').on('click', '#btn-search-product', btnSearchProductClick);

		/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
		function onPageLoading() {
			var vNumberProduct = getNumberProduct();
			$('#number-order-card').text(vNumberProduct);
			$('#number-order-card-mobile').text(vNumberProduct);
			$.get(`http://localhost:8080/productline`, showProductLineToSelectElement);
		}

		// hàm xử lý khi nhấn vào nút thêm đơn hàng
		function addProductOrderCart(paramproduct) {
			// khai báo đối tượng chứa dữ liệu
			// 1. thu thập dữ liệu
			var productOrder = {
				productId: $(paramproduct).attr('value'),
				numberProduct: 1
			};
			// 2. validate dữ liệu
			// 3. xử lý
			handleOrderCart(productOrder);
			var vNumberProduct = getNumberProduct();
			$('#number-order-card').text(vNumberProduct);
			$('#number-order-card-mobile').text(vNumberProduct);
		}

		// hàm xử lý khi nhấn vào nút thêm đơn hàng ở trang single-product
		function addSingleProductOrderCart() {
			// khai báo đối tượng chứa dữ liệu
			// 1. thu thập dữ liệu
			var productOrder = {
				productId: $('#add-product-order-cart').attr('value'),
				numberProduct: Number($('#number-product').val())
			};
			// 2. validate dữ liệu
			// 3. xử lý
			handleOrderCart(productOrder);
			var vNumberProduct = getNumberProduct();
			$('#number-order-card').text(vNumberProduct);
			$('#number-order-card-mobile').text(vNumberProduct);
		}

		// show data Customer vào select element
		function showProductLineToSelectElement(paramProductLine) {
			var vProductLineSelectElement = $('#search-type-product-line');
			for (var vI = 0; vI < paramProductLine.length; vI++) {
				$('<option/>', { text: paramProductLine[vI].productLine, value: paramProductLine[vI].id }).appendTo(
					vProductLineSelectElement
				);
			}
		}

		// hàm xử lý xự kiện khi thay đổi loại tìm kiếm
		function changeSelectTypeSearch() {
			gChangeTypeSearch = $('#select-type-search').val();
			if (gChangeTypeSearch == 1) {
				$('#search-type-price').css('display', 'block');
				$('#search-type-product-line').css('display', 'none');
				$('#search-type-product').css('display', 'none');
			}
			if (gChangeTypeSearch == 2) {
				$('#search-type-price').css('display', 'none');
				$('#search-type-product-line').css('display', 'block');
				$('#search-type-product').css('display', 'none');
			}
			if (gChangeTypeSearch == 3) {
				$('#search-type-price').css('display', 'none');
				$('#search-type-product-line').css('display', 'none');
				$('#search-type-product').css('display', 'block');
			}
		}

		// hàm xử lý khi nhấn nút tìm kiếm sản phẩm
		function btnSearchProductClick() {
			var vInfoSearch = {
				typeSearch: "",
				infoSearch: ""
			}
			getInfoSearch(vInfoSearch);
			if (vInfoSearch.infoSearch == "" || vInfoSearch.infoSearch == "0") {
				alert("bạn chưa nhập thông tin cần search")
			} else {
				localStorage.infoSearch = JSON.stringify(vInfoSearch);
				window.location.href = "search.html";
			}
		}

		/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

		// hàm xử lý giỏ hàng
		function handleOrderCart(paramProductOrder) {
			gArrayProduct = localStorage.infoOrder;
			var result = addProductToOrder(paramProductOrder);
			if (typeof Storage !== 'undefined') {
				localStorage.infoOrder = JSON.stringify(result);
				showAlert();
			} else {
				console.log('Sorry, your browser does not support web storage...');
			}
		}

		// hàm xử lý nếu chọn cùng sản phẩm thì tăng số lượng sản phẩm lên
		function addProductToOrder(paramProductOrder) {
			var vArrayInfoProduct = [];
			if (gArrayProduct != undefined) {
				vArrayInfoProduct = JSON.parse(gArrayProduct);
				var vI = 0;
				var vFound = false;
				while (!vFound && vI < vArrayInfoProduct.length) {
					if (vArrayInfoProduct[vI].productId == paramProductOrder.productId) {
						vFound = true;
						vArrayInfoProduct[vI].numberProduct =
							vArrayInfoProduct[vI].numberProduct + paramProductOrder.numberProduct;
					} else vI++;
				}
				if (!vFound) {
					vArrayInfoProduct.push(paramProductOrder);
				}
			} else {
				vArrayInfoProduct.push(paramProductOrder);
			}
			return vArrayInfoProduct;
		}

		// hàm xử lý lấy số lượng sản phẩm order
		function getNumberProduct() {
			var vArrayProduct = localStorage.infoOrder;
			var vNumberProduct = 0;
			if (vArrayProduct != undefined) {
				var vArrayInfoProduct = JSON.parse(vArrayProduct);
				for (var vI = 0; vI < vArrayInfoProduct.length; vI++) {
					vNumberProduct = vNumberProduct + vArrayInfoProduct[vI].numberProduct;
				}
				console.log(vNumberProduct);
				return vNumberProduct;
			}
			return vNumberProduct;
		}

		// hàm xử lý lấy thông tin cần tìm kiếm
		function getInfoSearch(paramInfoSearch) {
			if (gChangeTypeSearch == 1) {
				paramInfoSearch.typeSearch = 1;
				paramInfoSearch.infoSearch = $('#search-type-price').val();
			}
			if (gChangeTypeSearch == 2) {
				paramInfoSearch.typeSearch = 2;
				paramInfoSearch.infoSearch = $('#search-type-product-line').val();
			}
			if (gChangeTypeSearch == 3) {
				paramInfoSearch.typeSearch = 3;
				paramInfoSearch.infoSearch = $('#search-type-product').val();
			}
		}

		// hàm xử lý hiển thị khi add to card thành công
		function showAlert() {
			Swal.fire({
				position: 'top-end',
				icon: 'success',
				title: 'bạn đã lưu vào giỏ hàng thành công',
				showConfirmButton: false,
				timer: 1500
			  })
		}





		$('#product-order-cart').on('click', onAddToCardClick);

		function onAddToCardClick() {
			alert('bạn muốn add to card');
		}

		// testimonial sliders
		$('.testimonial-sliders').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			responsive: {
				0: {
					items: 1,
					nav: false
				},
				600: {
					items: 1,
					nav: false
				},
				1000: {
					items: 1,
					nav: false,
					loop: true
				}
			}
		});

		// homepage slider
		$('.homepage-slider').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			nav: true,
			dots: false,
			navText: [ '<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>' ],
			responsive: {
				0: {
					items: 1,
					nav: false,
					loop: true
				},
				600: {
					items: 1,
					nav: true,
					loop: true
				},
				1000: {
					items: 1,
					nav: true,
					loop: true
				}
			}
		});

		// logo carousel
		$('.logo-carousel-inner').owlCarousel({
			items: 4,
			loop: true,
			autoplay: true,
			margin: 30,
			responsive: {
				0: {
					items: 1,
					nav: false
				},
				480: {
					items: 2,
					nav: false
				},
				768: {
					items: 3,
					nav: false
				},
				1000: {
					items: 4,
					nav: false,
					loop: true
				}
			}
		});

		// count down
		if ($('.time-countdown').length) {
			$('.time-countdown').each(function() {
				var $this = $(this),
					finalDate = $(this).data('countdown');
				$this.countdown(finalDate, function(event) {
					var $this = $(this).html(
						event.strftime(
							'' +
								'<div class="counter-column"><div class="inner"><span class="count">%D</span>Days</div></div> ' +
								'<div class="counter-column"><div class="inner"><span class="count">%H</span>Hours</div></div>  ' +
								'<div class="counter-column"><div class="inner"><span class="count">%M</span>Mins</div></div>  ' +
								'<div class="counter-column"><div class="inner"><span class="count">%S</span>Secs</div></div>'
						)
					);
				});
			});
		}
		/*
		// projects filters isotop
		$('.product-filters #info-filter').on('click', 'li', function() {
			$('.product-filters li').removeClass('active');
			$(this).addClass('active');

			var selector = $(this).attr('data-filter');

			$('.product-lists').isotope({
				filter: selector
			});
		});
		*/
		
		// isotop inner
		$('.product-lists').isotope();

		// magnific popup
		$('.popup-youtube').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});

		// light box
		$('.image-popup-vertical-fit').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
		});

		// homepage slides animations
		$('.homepage-slider').on('translate.owl.carousel', function() {
			$('.hero-text-tablecell .subtitle').removeClass('animated fadeInUp').css({ opacity: '0' });
			$('.hero-text-tablecell h1')
				.removeClass('animated fadeInUp')
				.css({ opacity: '0', 'animation-delay': '0.3s' });
			$('.hero-btns').removeClass('animated fadeInUp').css({ opacity: '0', 'animation-delay': '0.5s' });
		});

		$('.homepage-slider').on('translated.owl.carousel', function() {
			$('.hero-text-tablecell .subtitle').addClass('animated fadeInUp').css({ opacity: '0' });
			$('.hero-text-tablecell h1').addClass('animated fadeInUp').css({ opacity: '0', 'animation-delay': '0.3s' });
			$('.hero-btns').addClass('animated fadeInUp').css({ opacity: '0', 'animation-delay': '0.5s' });
		});

		// stikcy js
		$('#sticker').sticky({
			topSpacing: 0
		});

		//mean menu
		$('.main-menu').meanmenu({
			meanMenuContainer: '.mobile-menu',
			meanScreenWidth: '992'
		});

		// search form
		$('.search-bar-icon').on('click', function() {
			$('.search-area').addClass('search-active');
		});

		$('.close-btn').on('click', function() {
			$('.search-area').removeClass('search-active');
		});
	});

	jQuery(window).on('load', function() {
		jQuery('.loader').fadeOut(1000);
	});
})(jQuery);
