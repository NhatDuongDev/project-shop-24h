/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//var gJsonArrayProduct = localStorage.infoOrder;
var gObjArrayProduct = [];
var gAmmount = 0;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
getInfoProductOrder();

$('#place-order').on('click', onBtnPlaceOrderClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý hiển thị table
function getInfoProductOrder() {
	if (localStorage.infoOrder != undefined) {
		$('#check-out-table > .order-details-body').children().remove();
		var vArrayProduct = JSON.parse(localStorage.infoOrder);
		var vTable = $('#check-out-table .order-details-body');
		var totalPriceOrder = 0;
		for (var vI = 0; vI < vArrayProduct.length; vI++) {
			$.ajax({
				url: 'http://localhost:8080/product/' + vArrayProduct[vI].productId,
				method: 'GET',
				async: false,
				success: function(pObjRes) {
					var vOrderDetail = {
						quantityOrder: vArrayProduct[vI].numberProduct,
						priceEach: pObjRes.sellPrice,
						producId: vArrayProduct[vI].productId
					};
					gObjArrayProduct.push(vOrderDetail);
					var rowTable = $('<tr/>').appendTo(vTable);
					$('<td/>', { text: pObjRes.productName }).appendTo(rowTable);
					$('<td/>', {
						text: (vArrayProduct[vI].numberProduct * pObjRes.sellPrice).toLocaleString() + ' đ'
					}).appendTo(rowTable);

					totalPriceOrder = totalPriceOrder + vArrayProduct[vI].numberProduct * pObjRes.sellPrice;
				},
				error: function(pXhrObj) {
					console.log(pXhrObj);
				}
			});
		}
		gAmmount = totalPriceOrder;
		$('#total-price-order').text(totalPriceOrder.toLocaleString() + ' đ');
		$('#shipping-price-order').text('0 đ');
		$('#total-ship-order-price').text(totalPriceOrder.toLocaleString() + ' đ');
	}
}

// hàm xử lý khi nhấn nút gửi đơn hàng
function onBtnPlaceOrderClick() {
	// khai báo biến chứa dữ liệu
	var vInfoOrder = {
		lastname: '',
		firstname: '',
		phoneNumber: '',
		address: '',
		city: '',
		country: '',
		orders: [
			{
				comments: '',
				orderDetail: []
			}
		],
		payment: [
			{
				ammount: ''
			}
		]
	};

	// 1. thu thập dữ liệu
	getInfoOrder(vInfoOrder);
	// 2. validate dữ liệu
	var vIsValidate = validateInfoOrder(vInfoOrder);
	if (vIsValidate) {
		// 3. xử lý nghiệp vụ
		callApiCreateAllInfoOrder(vInfoOrder);
	}
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// xử lý lấy thông tin đơn hàng
function getInfoOrder(paramInfoOrder) {
	paramInfoOrder.lastname = $('#lastname').val();
	paramInfoOrder.firstname = $('#firstnam').val();
	paramInfoOrder.address = $('#address').val();
	paramInfoOrder.phoneNumber = $('#phonenumber').val();
	paramInfoOrder.city = $('#city').val();
	paramInfoOrder.country = $('#country').val();
	paramInfoOrder.payment[0].ammount = gAmmount;
	paramInfoOrder.orders[0].comments = $('#comment-order').val();
	paramInfoOrder.orders[0].orderDetail = gObjArrayProduct;
}

// validate dữ liệu
function validateInfoOrder(paramInfoOrder) {
	if (paramInfoOrder.lastname == '') {
		alert(' bạn chưa nhập last name');
		return false;
	}
	if (paramInfoOrder.firstname == '') {
		alert(' bạn chưa nhập first name');
		return false;
	}
	if (paramInfoOrder.address == '') {
		alert(' bạn chưa nhập địa chỉ');
		return false;
	}
	if (paramInfoOrder.phoneNumber == '') {
		alert(' bạn chưa nhập sđt');
		return false;
	}
	if (paramInfoOrder.city == '') {
		alert(' bạn chưa nhập city');
		return false;
	}
	if (paramInfoOrder.country == '') {
		alert(' bạn chưa nhập country');
		return false;
	}
	checkInfoCustomer(paramInfoOrder);

	return true;
}

// hàm xử lý kiểm tra thông tin người dùng đã có chưa
function checkInfoCustomer(paramInfoOrder) {
	$.ajax({
		url: 'http://localhost:8080/custometByPhoneNumber/' + paramInfoOrder.phoneNumber,
		method: 'GET',
		async: false,
		success: function(pObjRes) {
			console.log(pObjRes);
			if (
				paramInfoOrder.firstname.toLowerCase() != pObjRes.firstname.toLowerCase() ||
				paramInfoOrder.lastname.toLowerCase() != pObjRes.lastname.toLowerCase()
			) {
				alert('SĐT này đã đăng ký và thông tin đã được cập nhật lại');
				$.ajax({
					url: 'http://localhost:8080/customer/' + pObjRes.id,
					type: 'PUT',
					contentType: 'application/json', // added data type
					data: JSON.stringify(paramInfoOrder),
					async: false,
					success: function(res) {
						console.log(res);
					},
					error: function(err) {
						console.log(err.response);
					}
				});
			}
			console.log('đã tìm thấy');
		},
		error: function(pXhrObj) {
			console.log(pXhrObj);
			console.log('không tìm thấy');
		}
	});
}

// Gọi Api tạo tất cả thông tin Order
function callApiCreateAllInfoOrder(paramOrder) {
	console.log(paramOrder);
	$.ajax({
		url: 'http://localhost:8080/customerorder',
		type: 'POST',
		contentType: 'application/json', // added data type
		data: JSON.stringify(paramOrder),
		async: false,
		success: function(res) {
			console.log(res);
			alert('bạn đã đặt hàng thành công');
			localStorage.removeItem('infoOrder');
			var vNumberProduct = getNumberProduct();
			$('#number-order-card').text(vNumberProduct);
			window.location.href = "index_2.html";
		},
		error: function(err) {
			console.log(err.response);
		}
	});
}

// hàm xử lý lấy số lượng sản phẩm order
function getNumberProduct() {
	var vArrayProduct = localStorage.infoOrder;
	var vNumberProduct = 0;
	if (vArrayProduct != undefined) {
		var vArrayInfoProduct = JSON.parse(vArrayProduct);
		for (var vI = 0; vI < vArrayInfoProduct.length; vI++) {
			vNumberProduct = vNumberProduct + vArrayInfoProduct[vI].numberProduct;
		}
		return vNumberProduct;
	}
	return vNumberProduct;
}
