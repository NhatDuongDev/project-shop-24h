/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// biến chứa mảng các sản phẩm được chọn
var gArrayProduct = null;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();



/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    callApiGetDataProduct();
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// hàm xử lý load data sản phẩm lên trang chủ
function loadProductToShopFrontEnd(paramDataProduct) {
    var vProduct = $("#product-card");
    var vDiv0 = $("<div/>").addClass("row categories__slider owl-carousel").appendTo(vProduct);
    for (var bI = 0; bI < paramDataProduct.length; bI++) {
        var vDivCol = $("<div/>").addClass("categories__item set-bg text-center").appendTo(vDiv0);
        var vDiv1 = $("<div/>").addClass("single-product-item").appendTo(vDivCol);
        var vDiv2 = $("<div/>").addClass("product-image").appendTo(vDiv1);
        var vA1 = $("<a/>").attr("href", "single-product.html" + "?id=" + paramDataProduct[bI].id).appendTo(vDiv2);
        $("<img/>", { src: paramDataProduct[bI].productImage }).appendTo(vA1);
        $("<h3/>", { text: paramDataProduct[bI].productName }).appendTo(vDiv1);
        $("<span/>", { text: "kl: " + paramDataProduct[bI].productWeight + "g" }).appendTo(vDiv1);
        $("<p/>", { text: paramDataProduct[bI].sellPrice.toLocaleString() + " đ", class: "product-price" }).appendTo(vDiv1);
        var vA2 = $("<a/>", { class: "cart-btn add-product-order-cart", text: "Add to cart ", value: paramDataProduct[bI].id }).appendTo(vDiv1);
        $("<i/>").addClass("fas fa-shopping-cart").appendTo(vA2);
    }
    // Product sliders
    $(".categories__slider").owlCarousel({
        loop: true,
        margin: 15,
        items: 3,
        dots: false,
        nav: true,
        navText: ["<span class='fa fa-angle-left'><span/>", "<span class='fa fa-angle-right'><span/>"],
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1000,
        autoHeight: false,
        autoplay: true,
        responsive: {

            0: {
                items: 1,
            },

            480: {
                items: 2,
            },

            768: {
                items: 3,
            }
        }
    });
}

// call api load dữ liệu product
function callApiGetDataProduct() {
    $.ajax({
        url: "http://localhost:8080/product",
        method: "GET",
        async: false,
        success: function (pObjRes) {
            loadProductToShopFrontEnd(pObjRes);
        },
        error: function (pXhrObj) {
            console.log(pXhrObj);
        }
    });
}
