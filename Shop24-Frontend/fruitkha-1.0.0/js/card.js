/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//var gJsonArrayProduct = localStorage.infoOrder;
var gObjArrayProduct = [];


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
getInfoProductOrder();

$("#product-order-table tbody").on("change", ".number-product-order", function() {
    changeQuantityProduct(this)
});
$("#product-order-table tbody").on("click", ".fa-times-circle", function() {
    deleteProductOrder(this)
});


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý hiển thị table
function getInfoProductOrder() {
    if (localStorage.infoOrder != undefined) {
        $("#product-order-table > tbody:last").children().remove()
        var vArrayProduct = JSON.parse(localStorage.infoOrder);
        var vTable = $("#product-order-table tbody");
        var totalPriceOrder = 0;
        for (var vI = 0; vI < vArrayProduct.length; vI ++) {
            $.ajax({
                url: "http://localhost:8080/product/" + vArrayProduct[vI].productId,
                method: "GET",
                async: false,
                success: function (pObjRes) {
                    gObjArrayProduct.push(pObjRes)
                    var rowTable = $("<tr/>").addClass("table-body-row").appendTo(vTable);
                    var col1 = $("<td/>").appendTo(rowTable);
                    var a = $("<a/>", {href: "#",value: pObjRes.id}).appendTo(col1);
                    $("<i/>").addClass("fas fa-times-circle").appendTo(a);
                    var col2 = $("<td/>").addClass("product-image").appendTo(rowTable);
                    $("<img/>", {src: pObjRes.productImage}).appendTo(col2);
                    $("<td/>", {text: pObjRes.productName}).addClass("product-name").appendTo(rowTable);
                    $("<td/>", {text: pObjRes.sellPrice.toLocaleString() + " đ"}).addClass("product-price").appendTo(rowTable);
                    var col5 = $("<td/>").addClass("product-quantity").appendTo(rowTable);
                    $("<input/>", {type: "number", value: vArrayProduct[vI].numberProduct, class: "number-product-order"}).attr("min","1").appendTo(col5);
                    $("<td/>", {text: vArrayProduct[vI].numberProduct}).addClass("product-total").appendTo(rowTable);
                    totalPriceOrder = totalPriceOrder + (vArrayProduct[vI].numberProduct * pObjRes.sellPrice);
                },
                error: function (pXhrObj) {
                    console.log(pXhrObj);
                }
            });
        }
        $("#total-price-order").text(totalPriceOrder.toLocaleString() + " đ");
        $("#shipping-price-order").text( "0 đ");
        $("#total-ship-order-price").text(totalPriceOrder.toLocaleString() + " đ");
    }
}

// hàm xử lý khi nhấn vào nút thêm đơn hàng
function changeQuantityProduct(paramproduct) {
    // khai báo đối tượng chứa dữ liệu
    // 1. thu thập dữ liệu
    var productOrder = {
        productId: $(paramproduct).parents("tr").find("a").attr("value"),
        numberProduct: Number($(paramproduct).val()),
    }
    // 2. validate dữ liệu
    // 3. xử lý 
    var result = addProductToOrder(productOrder);
    handleOrderCart(result);
    var vNumberProduct = getNumberProduct();
    $("#number-order-card").text(vNumberProduct);
    getInfoProductOrder();
}

// hàm xử lý khi nhấn xóa product Order
function deleteProductOrder(paramproduct) {
    // 1. thu thập dữ liệu
    productId = $(paramproduct).parents("a").attr("value"),
    console.log(productId);
    // 2. validate dữ liệu
    // 3. xử lý sự kiện
    var result = handleDeleteProduct(productId);
    handleOrderCart(result);
    var vNumberProduct = getNumberProduct();
    $("#number-order-card").text(vNumberProduct);
    getInfoProductOrder();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
 // hàm xử lý giỏ hàng
 function handleOrderCart(paramResult) {
    if (typeof (Storage) !== "undefined") {
        localStorage.infoOrder = JSON.stringify(paramResult);
    } else {
        console.log("Sorry, your browser does not support web storage...");
    }
}

// hàm xử lý nếu chọn cùng sản phẩm thì tăng số lượng sản phẩm lên
function addProductToOrder(paramProductOrder) {
    var vArrayInfoProduct = [];
    if (localStorage.infoOrder != undefined) {
        vArrayInfoProduct = JSON.parse(localStorage.infoOrder);
        var vI = 0;
        var vFound = false;
        while (!vFound && vI < vArrayInfoProduct.length) {
            if (vArrayInfoProduct[vI].productId == paramProductOrder.productId) {
                vFound = true;
                vArrayInfoProduct[vI].numberProduct = paramProductOrder.numberProduct;
            } else vI++;
        }
    }
    return vArrayInfoProduct;
}

// hàm xử lý lấy số lượng sản phẩm order
function getNumberProduct() {
    var vArrayProduct = localStorage.infoOrder;
    var vNumberProduct = 0;
    if (vArrayProduct != undefined) {
        var vArrayInfoProduct = JSON.parse(vArrayProduct);
        for (var vI = 0; vI < vArrayInfoProduct.length; vI++) {
            vNumberProduct = vNumberProduct + vArrayInfoProduct[vI].numberProduct;
        }
        return vNumberProduct;
    }
    return vNumberProduct;
}

// xử lý xóa sản phẩm trong cart
function handleDeleteProduct(paramProductId) {
    var vArrayInfoProduct = [];
    if (localStorage.infoOrder != undefined) {
        vArrayInfoProduct = JSON.parse(localStorage.infoOrder);
        var vI = 0;
        var vFound = false;
        while (!vFound && vI < vArrayInfoProduct.length) {
            if (vArrayInfoProduct[vI].productId == paramProductId) {
                vFound = true;
                vArrayInfoProduct.splice(vI,1);
            } else vI++;
        }
    }
    return vArrayInfoProduct;
}