/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
	var vInfoSearch = JSON.parse(localStorage.infoSearch);
	console.log(vInfoSearch);
	if (vInfoSearch.typeSearch == 1) {
		$.ajax({
			url: 'http://localhost:8080/product',
			method: 'GET',
			async: false,
			success: function(pObjRes) {
				console.log(pObjRes);
				handleSearchPrice(pObjRes, vInfoSearch.infoSearch);
			},
			error: function(pXhrObj) {
				console.log(pXhrObj);
			}
		});
	} else if (vInfoSearch.typeSearch == 2) {
		$.get(`http://localhost:8080/productline/${vInfoSearch.infoSearch}/product`, loadProductToShopFrontEnd);
	} else if (vInfoSearch.typeSearch == 3) {
		$.ajax({
			url: 'http://localhost:8080/product',
			method: 'GET',
			async: false,
			success: function(pObjRes) {
				console.log(pObjRes);
				handleSearchProduct(pObjRes, vInfoSearch.infoSearch);
			},
			error: function(pXhrObj) {
				console.log(pXhrObj);
			}
		});
	}
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm xử lý lọc lại dữ liệu search price
function handleSearchPrice(paramObject, paramInfoSearch) {
	var vArrayFilter = [];
	for (var vI = 0; vI < paramObject.length; vI++) {
		if (paramObject[vI].sellPrice <= paramInfoSearch) {
			vArrayFilter.push(paramObject[vI]);
		}
	}
	loadProductToShopFrontEnd(vArrayFilter);
}

// hàm xử lý lọc lại dữ liệu search product
function handleSearchProduct(paramObject, paramInfoSearch) {
	var vArrayFilter = [];
	for (var vI = 0; vI < paramObject.length; vI++) {
		if (paramObject[vI].productName.toLowerCase() == paramInfoSearch.toLowerCase()) {
			vArrayFilter.push(paramObject[vI]);
		}
	}
	loadProductToShopFrontEnd(vArrayFilter);
}

// hàm xử lý hiển thị lên front-end
function loadProductToShopFrontEnd(paramDataProduct) {
	console.log(paramDataProduct);
	if (paramDataProduct.length != 0) {
		var vProduct = $('#product-card');
        vProduct.empty();
		var vDiv0 = $('<div/>').addClass('row').appendTo(vProduct);
		for (var bI = 0; bI < paramDataProduct.length; bI++) {
			var vDivCol = $('<div/>')
				.addClass('col-lg-4 col-md-6 text-center ')
				.appendTo(vDiv0);
			var vDiv1 = $('<div/>').addClass('single-product-item').appendTo(vDivCol);
			var vDiv2 = $('<div/>').addClass('product-image').appendTo(vDiv1);
			var vA1 = $('<a/>').attr('href', 'single-product.html' + '?id=' + paramDataProduct[bI].id).appendTo(vDiv2);
			$('<img/>', { src: paramDataProduct[bI].productImage }).appendTo(vA1);
			$('<h3/>', { text: paramDataProduct[bI].productName }).appendTo(vDiv1);
			$('<span/>', { text: paramDataProduct[bI].productWeight + 'g' }).appendTo(vDiv1);
			$('<p/>', {
				text: paramDataProduct[bI].sellPrice.toLocaleString() + ' đ',
				class: 'product-price'
			}).appendTo(vDiv1);
			var vA2 = $('<a/>', {
				class: 'cart-btn add-product-order-cart',
				text: 'Add to cart ',
				value: paramDataProduct[bI].id
			}).appendTo(vDiv1);
			$('<i/>').addClass('fas fa-shopping-cart').appendTo(vA2);
		}
	} else {
		$('#no-info-search').css('display', 'block');
	}
}
