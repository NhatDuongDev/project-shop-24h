/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */


/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();


$('#show-many-image').on('click', ".product__details__pic__slider img", function () {

    var imgurl = $(this).data('imgbigurl');
    var bigImg = $('.product__details__pic__item--large').attr('src');
    if (imgurl != bigImg) {
        $('.product__details__pic__item--large').attr({
            src: imgurl
        });
    }
});


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    loadDetailProductFrontend();
    $.get(`http://localhost:8080/product`, loadProductToShopFrontEnd);
}

// hàm xử lý hiển thị chi tiết của sản phẩm khi load trang
function loadDetailProductFrontend() {
    // B1 get data from query string
    var vUrlString = window.location.href; //đường đẫn gọi đến trang
    var vUrl = new URL(vUrlString);

    //B2: get parameters
    gProductId = vUrl.searchParams.get("id");
    // B3: gọi ApI
    console.log(gProductId);
    $.get(`http://localhost:8080/product/${gProductId}`, loadProductDetailToFrontEnd)
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm xử lý lấy dữ liệu hiển thị chi tiết sản phẩm lên frontend
function loadProductDetailToFrontEnd(paramProductDetail) {
    $("#product-image").attr("src", paramProductDetail.productImage);
    $("#product-name").text(paramProductDetail.productName);
    $("#product-weight").text(paramProductDetail.productWeight + " g");
    $("#sell-price-product").text(paramProductDetail.sellPrice.toLocaleString() + " đ");
    $("#product-description").text(paramProductDetail.productDescription);
    $("#add-product-order-cart").attr("value", paramProductDetail.id);
    var vManyImage = $("#show-many-image")
    var vDiv = $("<div/>").addClass("product__details__pic__slider owl-carousel mt-1").appendTo(vManyImage);
    for (var vI = 0; vI <paramProductDetail.images.length; vI ++) {
        $("<img/>", {"data-imgbigurl": paramProductDetail.images[vI].linkImage, class: "img-thumbnail", src: paramProductDetail.images[vI].linkImage}).appendTo(vDiv);
    }
    $(".product__details__pic__slider").owlCarousel({
        loop: true,
        margin: 20,
        items: 3,
        dots: true,
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true
    });
}

// hàm xử lý load data sản phẩm liên quan đến sản phẩm chi tiết
function loadProductToShopFrontEnd(paramDataProduct) {
    var vProduct = $("#product-card");
    var vDiv0 = $("<div/>").addClass("row categories__slider owl-carousel").appendTo(vProduct);
    for (var bI = 0; bI < paramDataProduct.length; bI++) {
        var vDivCol = $("<div/>").addClass("categories__item set-bg text-center").appendTo(vDiv0);
        var vDiv1 = $("<div/>").addClass("single-product-item").appendTo(vDivCol);
        var vDiv2 = $("<div/>").addClass("product-image").appendTo(vDiv1);
        var vA1 = $("<a/>").attr("href", "single-product.html" + "?id=" + paramDataProduct[bI].id).appendTo(vDiv2);
        $("<img/>", { src: paramDataProduct[bI].productImage }).appendTo(vA1);
        $("<h3/>", { text: paramDataProduct[bI].productName }).appendTo(vDiv1);
        $("<span/>", { text: paramDataProduct[bI].productWeight + "g" }).appendTo(vDiv1);
        $("<p/>", { text: paramDataProduct[bI].sellPrice.toLocaleString() + " đ", class: "product-price" }).appendTo(vDiv1);
        var vA2 = $("<a/>", { class: "cart-btn add-product-order-cart", text: "Add to cart ", value: paramDataProduct[bI].id }).appendTo(vDiv1);
        $("<i/>").addClass("fas fa-shopping-cart").appendTo(vA2);
    }
    // Product sliders
    $(".categories__slider").owlCarousel({
        loop: true,
        margin: 15,
        items: 3,
        dots: false,
        nav: true,
        navText: ["<span class='fa fa-angle-left'><span/>", "<span class='fa fa-angle-right'><span/>"],
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1000,
        autoHeight: false,
        autoplay: true,
        responsive: {
    
            0: {
                items: 1,
            },
    
            480: {
                items: 2,
            },
    
            768: {
                items: 3,
            }
        }
    });
}

