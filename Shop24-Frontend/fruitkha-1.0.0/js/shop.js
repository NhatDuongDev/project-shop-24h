/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const page = $('#pagination');
const nextPage = $('#next-page');
const prevPage = $('#prev-page');

const valuePage = {
	truncate: true,
	curPage: 1,
	numLinksTwoSide: 1,
	totalPages: 1
};

var gIdProductLine = 0;

page.on("click", function(e) {
	const ele = e.target;
	console.log(ele.dataset.page);
	if (ele.dataset.page) {
		const pageNumber = parseInt(e.target.dataset.page, 10);
		valuePage.curPage = pageNumber;
		pagination();
		handleButtonLeft();
		handleButtonRight();
	}
});

$('#prev-page').on("click", function(e) {
	handleButton(e.target);
});

$('#next-page').on("click", function(e) {
	handleButton(e.target);
});

// hàm xử lý chính tạo tất cả element số trang.
function pagination() {
	page.empty();
	const { totalPages, curPage, truncate, numLinksTwoSide: delta } = valuePage;
	const range = delta + 4; // use for handle visible number of links left side

	if (gIdProductLine == 0) {
		$.get(`http://localhost:8080/productPage?page=${curPage}`, loadProductToShopFrontEnd);
	} else {
		$.get(`http://localhost:8080/productline/${gIdProductLine}/productpage?page=${curPage}`, loadProductToShopFrontEnd);
	}
	
	let render = '';
	let renderTwoSide = '';
	let dot = `<li><a>...</a></li>`;
	let countTruncate = 0; // use for ellipsis - truncate left side or right side

	// use for truncate two side
	const numberTruncateLeft = curPage - delta;
	const numberTruncateRight = curPage + delta;

	let active = '';
	for (let pos = 1; pos <= totalPages; pos++) {
		active = pos === curPage ? 'active' : '';

		// truncate
		if (totalPages >= 2 * range - 1 && truncate) {
			if (numberTruncateLeft > 3 && numberTruncateRight < totalPages - 3 + 1) {
				// truncate 2 side
				if (pos >= numberTruncateLeft && pos <= numberTruncateRight) {
					renderTwoSide += renderPage(pos, active);
				}
			} else {
				// truncate left side or right side
				if (
					(curPage < range && pos <= range) ||
					(curPage > totalPages - range && pos >= totalPages - range + 1) ||
					pos === totalPages ||
					pos === 1
				) {
					render += renderPage(pos, active);
				} else {
					countTruncate++;
					if (countTruncate === 1) render += dot;
				}
			}
		} else {
			// not truncate
			render += renderPage(pos, active);
		}
	}

	if (renderTwoSide) {
		renderTwoSide = renderPage(1) + dot + renderTwoSide + dot + renderPage(totalPages);
		page.append(renderTwoSide);
	} else {
		page.append(render);

	}
}

// hàm xử lý action và set Page khi nhấp vào page
function renderPage(index, active = '') {
	return ` <li>
		  <a data-page="${index}" class="${active}" href="#">${index}</a>
	  </li>`;
}

// hàm xử lý khi nhấn nut Prev hay Next Page
function handleButton(element) {
	if (element.classList.contains('prev-page')) {
		valuePage.curPage--;
		handleButtonLeft();
		$('button.next-page').prop("disabled", false);
	} else if (element.classList.contains('next-page')) {
		valuePage.curPage++;
		handleButtonRight();
		$('button.prev-page').prop("disabled", false);
	}
	pagination();
}

// hầm xử lý hiển thị nút Prev Page
function handleButtonLeft() {
	if (valuePage.curPage === 1) {
		$('button.prev-page').prop("disabled", true);
	} else {
		$('button.prev-page').prop("disabled", false);
	}
}

// hầm xử lý hiển thị nút Next Page
function handleButtonRight() {
	if (valuePage.curPage === valuePage.totalPages) {
		$('button.next-page').prop("disabled", true);
	} else {
		$('button.next-page').prop("disabled", false);
	}
}

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

$('.product-filters #info-filter').on('click', 'li', function() {
	$('.product-filters li').removeClass('active');
	$(this).addClass('active');
	gIdProductLine = $(this).val();
	if (gIdProductLine == 0) {
		$.get(`http://localhost:8080/product`, handleShowPaging);	
	} else {
		$.get(`http://localhost:8080/productline/${gIdProductLine}/product`, handleShowPaging);
	}
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
	$.get(`http://localhost:8080/productline`, loadProductLineToFilter);
	$.get(`http://localhost:8080/product`, handleShowPaging);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm xử lý tạo bộ lộc sản phẩm
function loadProductLineToFilter(paramInfoFilter) {
	var vFilterElement = $('#info-filter');
	for (var vI = 0; vI < paramInfoFilter.length; vI++) {
		//var vFilter = paramInfoFilter[vI].productLine.split(' ')[0];
		$('<li/>', { text: paramInfoFilter[vI].productLine, value: paramInfoFilter[vI].id}).appendTo(vFilterElement);
	}
}

// hàm xử lý hiển thị phân trang
function handleShowPaging(paramDataProduct) {
	var vNumberPage = countNumberPage(paramDataProduct);
	valuePage.curPage = 1
	if (vNumberPage == 1) {
		$('button.next-page').prop("disabled", true);
		$('button.prev-page').prop("disabled", true);
	} else {
		$('button.next-page').prop("disabled", false);
		$('button.prev-page').prop("disabled", true);
	}
	valuePage.totalPages = vNumberPage;
	pagination();
}

// xử lý đếm số trang cần phân trang
function countNumberPage(paramDataProduct) {
	var vNumberPage = 0;
	if (paramDataProduct.length % 6 == 0) {
		vNumberPage = paramDataProduct.length / 6;
	} else {
		vNumberPage = Math.floor(paramDataProduct.length / 6) + 1;
	}
	return vNumberPage;
}

//hàm xử lý load sản phẩm vào cửa hàng
function loadProductToShopFrontEnd(paramDataProduct) {
	var vProduct = $('#product-card');
	vProduct.empty();
	var vDiv0 = $('<div/>').addClass('row product-lists').appendTo(vProduct);
	for (var bI = 0; bI < paramDataProduct.length; bI++) {
		var vFilter = paramDataProduct[bI].productName.split(' ')[0];
		var vDivCol = $('<div/>').addClass('col-lg-4 col-md-6 text-center ' + vFilter).appendTo(vDiv0);
		var vDiv1 = $('<div/>').addClass('single-product-item').appendTo(vDivCol);
		var vDiv2 = $('<div/>').addClass('product-image').appendTo(vDiv1);
		var vA1 = $('<a/>').attr('href', 'single-product.html' + '?id=' + paramDataProduct[bI].id).appendTo(vDiv2);
		$('<img/>', { src: paramDataProduct[bI].productImage }).appendTo(vA1);
		$('<h3/>', { text: paramDataProduct[bI].productName }).appendTo(vDiv1);
		$('<span/>', { text: paramDataProduct[bI].productWeight + 'g' }).appendTo(vDiv1);
		$('<p/>', { text: paramDataProduct[bI].sellPrice.toLocaleString() + ' đ', class: 'product-price' }).appendTo(
			vDiv1
		);
		var vA2 = $('<a/>', {
			class: 'cart-btn add-product-order-cart',
			text: 'Add to cart ',
			value: paramDataProduct[bI].id
		}).appendTo(vDiv1);
		$('<i/>').addClass('fas fa-shopping-cart').appendTo(vA2);
	}
}
