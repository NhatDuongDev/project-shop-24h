/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// khai báo biến chứa khi tha đổi
var gTypeReport = 1;
var gArrayAmmount = [];
var gArrayCustomer = [];

var areaChartData = {
	labels: gArrayCustomer,
	datasets: [
		{
			label: 'total',
			backgroundColor: 'green',
			borderColor: 'rgba(60,141,188,0.8)',
			pointRadius: false,
			pointColor: '#3b8bba',
			pointStrokeColor: 'rgba(60,141,188,1)',
			pointHighlightFill: '#fff',
			pointHighlightStroke: 'rgba(60,141,188,1)',
			data: gArrayAmmount
		}
	]
};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

$('#input-number-report').on('change', changeNumberReport);
$('#select-type-report').on('change', changeTypeReportOrder);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
	$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataCustomerAmmount/excel")
	$('#input-number-report').val('');
	callApiGetDataByDate('reportCustomerAmmount');
}

// hàm xử lý khi thay đổi so hiển thị
function changeNumberReport() {
	var vChangeDay = $('#input-number-report').val();
	console.log(vChangeDay);

	if (gTypeReport == 1) {
		var api = 'reportCustomerAmmount/' + vChangeDay;
		callApiGetDataByDate(api);
	} else if (gTypeReport == 2) {
		var api = 'reportCustomerOrder/' + vChangeDay;
		callApiGetDataByDate(api);
	}
}

// hàm xử lý khi thay đổi type hiển thị
function changeTypeReportOrder() {
	gTypeReport = $('#select-type-report').val();
	console.log(gTypeReport);

	if (gTypeReport == 1) {
		$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataCustomerAmmount/excel")
		var api = 'reportCustomerAmmount';
		callApiGetDataByDate(api);
		$('#input-number-report').val('');
	} else if (gTypeReport == 2) {
		$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataCustomerOrder/excel")
		var api = 'reportCustomerOrder';
		callApiGetDataByDate(api);
		$('#input-number-report').val('');
	}
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm load dữ liệu lên line chart
function loadDataToLineChart(paramArra) {
	$('#barChart').remove();
	$('#barChartFather').append(
		'<canvas id="barChart" style="min-height: 400px; height: 400px; max-height: 400px; max-width: 100%;"></canvas>'
	);

	areaChartData.datasets[0].data = [];
	areaChartData.labels = [];
	for (vI = 0; vI < paramArra.length; vI++) {
		gArrayCustomer.push(paramArra[vI].name);
		gArrayAmmount.push(paramArra[vI].total);
	}

	areaChartData.datasets[0].data = gArrayAmmount;
	areaChartData.labels = gArrayCustomer;
	//-------------
	//- BAR CHART -
	//-------------
	var barChartCanvas = $('#barChart').get(0).getContext('2d');
	var barChartData = $.extend(true, {}, areaChartData);
	var temp0 = areaChartData.datasets[0];
	barChartData.datasets[0] = temp0;

	var barChartOptions = {
		responsive: true,
		maintainAspectRatio: false,
		datasetFill: false
	};

	new Chart(barChartCanvas, {
		type: 'bar',
		data: barChartData,
		options: barChartOptions
	});
}

// hàm gọi api lấy dữ liệu lọc theo ngày
function callApiGetDataByDate(paramAPI) {
	$.ajax({
		url: 'http://localhost:8080/' + paramAPI,
		method: 'GET',
		async: false,
		success: function(pObjRes) {
			gArrayAmmount = [];
			gArrayCustomer = [];
			loadDataToLineChart(pObjRes);
		},
		error: function(pXhrObj) {
			console.log(pXhrObj);
		}
	});
}
