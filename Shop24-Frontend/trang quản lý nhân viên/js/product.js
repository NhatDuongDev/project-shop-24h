/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khởi tạo DataTable, gán data và mapping các columns
var gProductTable = $('#product-table').DataTable({
	columns: [
		{ data: 'id' },
		{ data: 'productCode' },
		{ data: 'productName' },
		{ data: 'productImage' },
		{ data: 'productDescription' },
		{ data: 'sellPrice' },
		{ data: 'productVendor' },
		{ data: 'productWeight' },
		{ data: 'quantityInStock' },
		{ data: 'buyPrice' },
		{ data: 'action' }
	],
	columnDefs: [
		{
			targets: 10,
			defaultContent: `
        <a href="#" class="fa-lg mr-2"><i class="fas fa-edit text-primary"></i></a>| 
		<a href="#" class="fa-lg ml-2"><i class="fas fa-trash text-danger"></i></a>`
		}
	]
});

// Khai báo biến hằng lưu trữ trạng thái form
const gFORM_MODE_NORMAL = 'Normal';
const gFORM_MODE_INSERT = 'Insert';
const gFORM_MODE_UPDATE = 'Update';
const gFORM_MODE_DELETE = 'Delete';
// Biến toàn cục lưu trạng thái form, mặc định NORMAL
var gFormMode = gFORM_MODE_NORMAL;

// biến chứa id
var gProductLineId = 0;
var gProductId = 0;

// biến chứa data album
var gProduct = [];

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

onPageLoading();

$('#select-product-line').change(onGetProductChange);

$('#create-product').on('click', onBtnCreateProductClick);
$('#btn-modal-save-product').on('click', onModalBtnSaveProductClick);
$('#delete-all-product').on('click', onBtnDeleteAllProductClick);
$('#product-table tbody').on('click', '.fa-edit', function() {
	onIconEditClick(this);
});
$('#product-table tbody').on('click', '.fa-trash', function() {
	onIconDeleteClick(this);
});

$('#modal-btn-delete-all-product').on('click', callApiDeleteAllProduct);
$('#modal-btn-delete-product').on('click', callApiDeleteProductById);

// reset form khi hidden modal
$('#modal-info-product').on('hidden.bs.modal', resetFormToStart);
$('#modal-delete-product').on('hidden.bs.modal', resetFormToStart);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
	$.get(`http://localhost:8080/productline`, showProductLineToSelectElement);
	$.get(`http://localhost:8080/product`, loadProductToTable);
}

// hàm xử lý khi nhấn nút create album
function onBtnCreateProductClick() {
	gFormMode = gFORM_MODE_INSERT;
	$('#modal-input-product-code').val('');
	$('#modal-input-product-name').val('');
	$('#modal-input-product-description').val('');
	$('#modal-input-product-sell-price').val('');
	$('#modal-input-product-vendor').val('');
	$('#modal-input-product-weight').val('');
	$('#modal-input-quantity').val('');
	$('#modal-input-buy-price').val('');
	$('#modal-info-product').modal('show');
}

// hàm xử lý khi nhấn vào icon edit
function onIconEditClick(paramIconEdit) {
	gFormMode = gFORM_MODE_UPDATE;
	gProductId = $(paramIconEdit).parents('tr').find('td:first').html();
	$.get(`http://localhost:8080/product/${gProductId}`, showInfoProductToModal);
}

// hàm xử lý khi nhấn vào icon delete
function onIconDeleteClick(paramIconDelete) {
	gFormMode = gFORM_MODE_DELETE;
	gProductId = $(paramIconDelete).parents('tr').find('td:first').html();
	$('#modal-delete-product').modal('show');
}

// hàm xử lý khi nhấn nút save trên modal
function onModalBtnSaveProductClick() {
	// biến chưa dữ liệu info payment
	var vInfoProduct = {
		productCode: '',
		productName: '',
		productImage: '',
		productDescription: '',
		sellPrice: '',
		productVendor: '',
		productWeight: '',
		quantityInStock: '',
		buyPrice: ''
	};
	// 1: thu thập dữ liệu
	getInfoProduct(vInfoProduct);
	// 2: validate dữ liệu
	var vIsValidate = validateInfoProduct(vInfoProduct);
	if (vIsValidate) {
		// 3: xử lý nghiệp vụ
		handleInfoProduct(vInfoProduct);
	}
}

// hàm xử lý khi nhấn nut delete all album
function onBtnDeleteAllProductClick() {
	$('#modal-delete-all-product').modal('show');
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load dữ liệu data lên album table
function loadProductToTable(paramProduct) {
	gProduct = paramProduct;
	gProductTable.clear();
	gProductTable.rows.add(paramProduct);
	gProductTable.draw();
}

// show data Customer vào select element
function showProductLineToSelectElement(paramProductLine) {
	var vProductLineSelectElement = $('#select-product-line');
	for (var vI = 0; vI < paramProductLine.length; vI++) {
		$('<option/>', { text: paramProductLine[vI].productLine, value: paramProductLine[vI].id }).appendTo(
			vProductLineSelectElement
		);
	}
}

// hàm xử lý khi thay đổi customer sẽ load ra table payment tương ứng
function onGetProductChange(event) {
	gProductLineId = event.target.value;
	console.log(gProductLineId);
	if (gProductLineId == 0) {
		$.get(`http://localhost:8080/product`, loadProductToTable);
	} else {
		$.get(`http://localhost:8080/productline/${gProductLineId}/product`, loadProductToTable);
	}
}

// hàm thu thập dữ liệu để tạo hoặc update album
function getInfoProduct(paramProduct) {
	paramProduct.productCode = $('#modal-input-product-code').val();
	paramProduct.productName = $('#modal-input-product-name').val();
	paramProduct.productImage = $('#modal-input-product-image').val();
	paramProduct.productDescription = $('#modal-input-product-description').val();
	paramProduct.sellPrice = $('#modal-input-product-sell-price').val();
	paramProduct.productVendor = $('#modal-input-product-vendor').val();
	paramProduct.productWeight = $('#modal-input-product-weight').val();
	paramProduct.quantityInStock = $('#modal-input-quantity').val();
	paramProduct.buyPrice = $('#modal-input-buy-price').val();
}

// hàm validate dữ liệu thu thập được
function validateInfoProduct(paramProduct) {
	if (gProductLineId == 0 && gFormMode == gFORM_MODE_INSERT) {
		alert('Hãy chọn 1 product line để tạo product');
		return false;
	}
	if (paramProduct.productCode == '') {
		alert('không được để trống Product Code');
		return false;
	}
	if (checkProductCode(paramProduct.productCode) && gFormMode == gFORM_MODE_INSERT) {
		alert('Product Code đã bị trùng');
		return false;
	}
	if (paramProduct.productName == '') {
		alert('không được để trống Product Name');
		return false;
	}
	if (paramProduct.productImage == '') {
		alert('không được để trống Product Image');
		return false;
	}
	if (paramProduct.productDescription == '') {
		alert('không được để trống product description');
		return false;
	}
	if (paramProduct.sellPrice == '') {
		alert('không được để trống product sell price');
		return false;
	}
	if (paramProduct.productVendor == '') {
		alert('không được để trống Product vendor');
		return false;
	}
	if (paramProduct.productWeight == '') {
		alert('không được để trống Product weight');
		return false;
	}
	if (paramProduct.quantityInStock == '') {
		alert('không được để trống quantity in stock');
		return false;
	}
	if (paramProduct.buyPrice == '') {
		alert('không được để trống buy price');
		return false;
	}
	return true;
}

// hàm kiểm tra product code có bị trùng ko
function checkProductCode(paramProductCode) {
	var vI = 0;
	var vFound = false;
    while (!vFound && vI < gProduct.length) {
        if (gProduct[vI].productCode == paramProductCode) {
            vFound = true;
        } else vI ++;
    }
    return vFound;
}

// hàm xử lý create or update payment
function handleInfoProduct(paramProduct) {
	if (gFormMode === gFORM_MODE_INSERT) {
		callApiCreateProduct(paramProduct);
	} else if (gFormMode === gFORM_MODE_UPDATE) {
		callApiUpdateProduct(paramProduct);
	}
}

// hàm hiển thị thông tin album ra font-end
function showInfoProductToModal(paramProductDetail) {
	$('#modal-input-product-code').val(paramProductDetail.productCode);
	$('#modal-input-product-name').val(paramProductDetail.productName);
	$('#modal-input-product-image').val(paramProductDetail.productImage);
	$('#modal-input-product-description').val(paramProductDetail.productDescription);
	$('#modal-input-product-sell-price').val(paramProductDetail.sellPrice);
	$('#modal-input-product-vendor').val(paramProductDetail.productVendor);
	$('#modal-input-product-weight').val(paramProductDetail.productWeight);
	$('#modal-input-quantity').val(paramProductDetail.quantityInStock);
	$('#modal-input-buy-price').val(paramProductDetail.buyPrice);
	$('#modal-info-product').modal('show');
}

/**
   * Hàm reset form về lại mặc định
   * Input: form còn chứa dữ liệu
   * Output: form rỗng
   */
function resetFormToStart() {
	gFormMode = gFORM_MODE_NORMAL;
	gProductId = 0;
	$('#modal-input-product-code').val('');
	$('#modal-input-product-name').val('');
	$('#modal-input-product-image').val('');
	$('#modal-input-product-description').val('');
	$('#modal-input-product-sell-price').val('');
	$('#modal-input-product-vendor').val('');
	$('#modal-input-product-weight').val('');
	$('#modal-input-quantity').val('');
	$('#modal-input-buy-price').val('');
}

// call api create album
function callApiCreateProduct(paramCustomer) {
	$.ajax({
		url: 'http://localhost:8080/productline/' + gProductLineId + '/product',
		type: 'POST',
		contentType: 'application/json', // added data type
		data: JSON.stringify(paramCustomer),
		success: function(res) {
			console.log(res);
			alert(`Đã tạo thành công product`);
			$('#modal-info-product').modal('hide');
			resetFormToStart();
			$.get(`http://localhost:8080/productline/${gProductLineId}/product`, loadProductToTable);
		},
		error: function(err) {
			console.log(err.response);
		}
	});
}

// call api update dữ liệu
function callApiUpdateProduct(paramPayment) {
	$.ajax({
		url: 'http://localhost:8080/product/' + gProductId,
		type: 'PUT',
		contentType: 'application/json', // added data type
		data: JSON.stringify(paramPayment),
		success: function(res) {
			console.log(res);
			alert(`Đã update thành công product`);
			$('#modal-info-product').modal('hide');
			resetFormToStart();
			if (gProductLineId == 0) {
				$.get(`http://localhost:8080/product`, loadProductToTable);
			} else {
				$.get(`http://localhost:8080/productline/${gProductLineId}/product`, loadProductToTable);
			}
		},
		error: function(err) {
			console.log(err.response);
		}
	});
}

// call api delete all album
function callApiDeleteAllProduct() {
	$.ajax({
		url: 'http://localhost:8080/product',
		type: 'DELETE',
		dataType: 'json',
		success: function(res) {
			alert('Successfully Delete All product');
			$('#modal-delete-all-product').modal('hide');
			location.reload();
		},
		error: function(error) {
			console.assert(error.responseText);
		}
	});
}

// call api delete album by id
function callApiDeleteProductById() {
	$.ajax({
		url: 'http://localhost:8080/product/' + gProductId,
		type: 'DELETE',
		dataType: 'json',
		success: function(res) {
			alert('Successfully Delete product');
			$('#modal-delete-product').modal('hide');
			resetFormToStart();
			if (gProductLineId == 0) {
				$.get(`http://localhost:8080/product`, loadProductToTable);
			} else {
				$.get(`http://localhost:8080/productline/${gProductLineId}/product`, loadProductToTable);
			}
		},
		error: function(error) {
			console.assert(error.responseText);
		}
	});
}
