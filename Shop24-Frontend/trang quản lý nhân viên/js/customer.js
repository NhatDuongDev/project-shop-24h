/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khởi tạo DataTable, gán data và mapping các columns
var gCustomerTable = $('#customer-table').DataTable({
    columns: [
        { data: 'id' },
        { data: 'lastname' },
        { data: 'firstname' },
        { data: 'phoneNumber' },
        { data: 'address' },
        { data: 'city' },
        { data: 'state' },
        { data: 'postalCode' },
        { data: 'country' },
        { data: 'salesRepEmployeeNumber' },
        { data: 'creditLimit' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 11,
            defaultContent: `
        <a href="#" class="fa-lg mr-2"><i class="fas fa-edit text-primary"></i></a>` 
		//<a href="#" class="fa-lg ml-2"><i class="fas fa-trash text-danger"></i></a>,
        },
    ],
});

// biến chưa dữ liệu info album
var gInfoCustomer = {
    lastname: "",
    firstname: "",
    phoneNumber: "",
    address: "",
    city: "",
    state: "",
    postalCode: "",
    country: "",
    salesRepEmployeeNumber: "",
    creditLimit: "",
}

// biến chứa data album
var gCustomer = [];
var gMode = 0;

// biến chứa id album
var gIdCustomer = 0;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

$("#create-customer").on("click", onBtnCreateCustomerClick);
$("#update-customer").on("click", onBtnUpdateCustomerClick);
$("#delete-all-customer").on("click", onBtnDeleteAllCustomerClick);
$("#customer-table tbody").on("click",".fa-edit", function() {
    onIconEditClick(this);
});
$("#customer-table tbody").on("click",".fa-trash", function() {
    onIconDeleteClick(this);
});

$("#modal-btn-delete-all-customer").on("click", callApiDeleteAllCustomer);
$("#modal-btn-delete-customer").on("click", callApiDeleteCustomerById);


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.get(`http://localhost:8080/customer`, loadAlbumToTable);
}

// hàm xử lý khi nhấn nút create album
function onBtnCreateCustomerClick() {
    // 1: thu thập dữ liệu
    getInfoCustomer();
    // 2: validate dữ liệu
    var vIsValidate = validateInfoCustomer();
    if (vIsValidate) {
        // 3: xử lý nghiệp vụ
        callApiCreateCustomer(gInfoCustomer);
    }
}

// hàm xử lý khi nhấn vào icon edit
function onIconEditClick(paramIconEdit) {
    gIdCustomer = $(paramIconEdit).parents("tr").find("td:first").html();
    $.get(`http://localhost:8080/customer/${gIdCustomer}`, showInfoCustomer);
    $("#create-customer").prop("disabled",  true);
    $("#delete-all-customer").prop("disabled",  true);
}

// hàm xử lý khi nhấn vào icon delete
function onIconDeleteClick(paramIconDelete) {
    gIdCustomer = $(paramIconDelete).parents("tr").find("td:first").html();
    $("#modal-delete-customer").modal("show");
}

// hàm xử lý khi nhấn nút update album
function onBtnUpdateCustomerClick() {
    gMode = 1;
    // 1: thu thập dữ liệu
    getInfoCustomer();
    // 2: validate dữ liệu
    var vIsValidate = validateInfoCustomer();
    if (vIsValidate) {
        // 3: xử lý nghiệp vụ
        callApiUpdateCustomer(gInfoCustomer);
    }
}

// hàm xử lý khi nhấn nut delete all album
function onBtnDeleteAllCustomerClick() {
    $("#modal-delete-all-customer").modal("show");
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load dữ liệu data lên album table
function loadAlbumToTable(paramCustomer) {
    gCustomer = paramCustomer;
    gCustomerTable.clear();
    gCustomerTable.rows.add(paramCustomer);
    gCustomerTable.draw();
}

// hàm thu thập dữ liệu để tạo hoặc update album
function getInfoCustomer() {
    gInfoCustomer.lastname = $("#input-lastname").val();
    gInfoCustomer.firstname = $("#input-firstname").val();
    gInfoCustomer.phoneNumber = $("#input-phone-number").val();
    gInfoCustomer.address = $("#input-address").val();
    gInfoCustomer.city = $("#input-city").val();
    gInfoCustomer.state = $("#input-state").val();
    gInfoCustomer.postalCode = $("#input-postal-code").val();
    gInfoCustomer.country = $("#input-country").val();
    gInfoCustomer.salesRepEmployeeNumber = $("#input-employee-number").val();
    gInfoCustomer.creditLimit = $("#input-credit-limit").val();
}

// hàm validate dữ liệu thu thập được
function validateInfoCustomer() {
    if (gInfoCustomer.lastname == "") {
        alert("không được để trống lastname");
        return false;
    }
    if (gInfoCustomer.firstname == "") {
        alert("không được để trống firstname");
        return false;
    }
    if (gInfoCustomer.phoneNumber == "") {
        alert("không được để trống phone number");
        return false;
    }
    if (checkPhoneNumber(gInfoCustomer.phoneNumber) && gMode != 1) {
		alert('Phone Number đã bị trùng');
		return false;
	}
    if (gInfoCustomer.address == "") {
        alert("không được để trống address");
        return false;
    }
    if (gInfoCustomer.city == "") {
        alert("không được để trống city");
        return false;
    }
    if (gInfoCustomer.country == "") {
        alert("không được để trống country");
        return false;
    }
    return true;
}

// hàm kiểm tra phone number có bị trùng ko
function checkPhoneNumber(paramPhoneNumber) {
	var vI = 0;
	var vFound = false;
    while (!vFound && vI < gCustomer.length) {
        if (gCustomer[vI].phoneNumber == paramPhoneNumber) {
            vFound = true;
        } else vI ++;
    }
    return vFound;
}

// hàm hiển thị thông tin album ra font-end
function showInfoCustomer(paramCustomer) {
    $("#input-lastname").val(paramCustomer.lastname);
    $("#input-firstname").val(paramCustomer.firstname);
    $("#input-phone-number").val(paramCustomer.phoneNumber);
    $("#input-address").val(paramCustomer.address);
    $("#input-city").val(paramCustomer.city);
    $("#input-state").val(paramCustomer.state);
    $("#input-postal-code").val(paramCustomer.postalCode);
    $("#input-country").val(paramCustomer.country);
    $("#input-employee-number").val(paramCustomer.salesRepEmployeeNumber);
    $("#input-credit-limit").val(paramCustomer.creditLimit);
}

// call api create album
function callApiCreateCustomer(paramCustomer) {
    $.ajax({
        url: "http://localhost:8080/customer",
        type: "POST",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramCustomer),
        success: function (res) {
            console.log(res);
            alert(`Đã tạo thành công customer`);
            location.reload();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api update dữ liệu
function callApiUpdateCustomer(paramCustomer) {
    $.ajax({
        url: "http://localhost:8080/customer/" + gIdCustomer,
        type: "PUT",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramCustomer),
        success: function (res) {
            console.log(res);
            alert(`Đã update thành công customer`);
            location.reload();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api delete all album
function callApiDeleteAllCustomer() {
    $.ajax({
        url: "http://localhost:8080/customer",
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete All Customer");
            $("#modal-delete-all-customer").modal("hide");
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// call api delete album by id
function callApiDeleteCustomerById() {
    $.ajax({
        url: "http://localhost:8080/customer/" + gIdCustomer,
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete Customer");
            $("#modal-delete-customer").modal("hide");
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}