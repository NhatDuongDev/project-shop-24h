/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khởi tạo DataTable, gán data và mapping các columns
var gOrderTable = $('#order-table').DataTable({
    columns: [
        { data: 'id' },
        { data: 'orderDate' },
        { data: 'requiredDate' },
        { data: 'shippedDate' },
        { data: 'status' },
        { data: 'comments' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 6,
            defaultContent: `
        <a href="#" class="fa-lg mr-2"><i class="fas fa-edit text-primary"></i></a>` 
		//<a href="#" class="fa-lg ml-2"><i class="fas fa-trash text-danger"></i></a>`,
        },
    ],
});

// Khai báo biến hằng lưu trữ trạng thái form
const gFORM_MODE_NORMAL = 'Normal';
const gFORM_MODE_INSERT = 'Insert';
const gFORM_MODE_UPDATE = 'Update';
const gFORM_MODE_DELETE = 'Delete';
// Biến toàn cục lưu trạng thái form, mặc định NORMAL
var gFormMode = gFORM_MODE_NORMAL;

// biến chứa id
var gCustomerId = 0;
var gOrderId = 0;

// biến chứa data album
var gCustomer = [];



/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

onPageLoading();

$("#select-customer").change(onGetOrderChange);

$("#create-order").on("click", onBtnCreateOrderClick);
$("#btn-modal-save-order").on("click", onModalBtnSaveOrderClick);
$("#delete-all-order").on("click", onBtnDeleteAllOrderClick);
$("#order-table tbody").on("click", ".fa-edit", function () {
    onIconEditClick(this);
});
$("#order-table tbody").on("click", ".fa-trash", function () {
    onIconDeleteClick(this);
});

$("#modal-btn-delete-all-order").on("click", callApiDeleteAllOrder);
$("#modal-btn-delete-order").on("click", callApiDeleteOrderById);

// reset form khi hidden modal
$('#modal-info-order').on('hidden.bs.modal', resetFormToStart);
$('#modal-delete-order').on('hidden.bs.modal', resetFormToStart);


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.get(`http://localhost:8080/customerSelect`, showCustomerToSelectElement);
    $.get(`http://localhost:8080/order`, loadOrderToTable);
}

// hàm xử lý khi nhấn nút create album
function onBtnCreateOrderClick() {
    gFormMode = gFORM_MODE_INSERT;
    $("#modal-input-order-date").val("");
    $("#modal-input-required-date").val("");
    $("#modal-input-shipped-date").val("");
    $("#modal-input-status").val("");
    $("#modal-input-comment").val("");
    $("#modal-info-order").modal("show");
}

// hàm xử lý khi nhấn vào icon edit
function onIconEditClick(paramIconEdit) {
    gFormMode = gFORM_MODE_UPDATE;
    gOrderId = $(paramIconEdit).parents("tr").find("td:first").html();
    $.get(`http://localhost:8080/order/${gOrderId}`, showInfoOrderToModal);
}

// hàm xử lý khi nhấn vào icon delete
function onIconDeleteClick(paramIconDelete) {
    gFormMode = gFORM_MODE_DELETE;
    gOrderId = $(paramIconDelete).parents("tr").find("td:first").html();
    $("#modal-delete-order").modal("show");
}

// hàm xử lý khi nhấn nút save trên modal
function onModalBtnSaveOrderClick() {
    // biến chưa dữ liệu info payment
    var vInfoOrder = {
        orderDate: "",
        requiredDate: "",
        shippedDate: "",
        status: "",
        comments: "",
    }
    // 1: thu thập dữ liệu
    getInfoOrder(vInfoOrder);
    // 2: validate dữ liệu
    var vIsValidate = validateInfoOrder(vInfoOrder);
    if (vIsValidate) {
        // 3: xử lý nghiệp vụ
        handleInfoOrder(vInfoOrder);
    }
}

// hàm xử lý khi nhấn nut delete all album
function onBtnDeleteAllOrderClick() {
    $("#modal-delete-all-order").modal("show");
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load dữ liệu data lên album table
function loadOrderToTable(paramOrder) {
    gOrderTable.clear();
    gOrderTable.rows.add(paramOrder);
    gOrderTable.draw();
}

// show data Customer vào select element
function showCustomerToSelectElement(paramCustomer) {
    var vCustomerSelectElement = $("#select-customer");
    for (var vI = 0; vI < paramCustomer.length; vI++) {
        $("<option/>", { text: paramCustomer[vI][0] + " - " + paramCustomer[vI][1] + " " + paramCustomer[vI][2], value: paramCustomer[vI][0] }).appendTo(vCustomerSelectElement);
    }
}

// hàm xử lý khi thay đổi customer sẽ load ra table payment tương ứng
function onGetOrderChange(event) {
    gCustomerId = event.target.value;
    console.log(gCustomerId);
    if (gCustomerId == 0) {
        $.get(`http://localhost:8080/order`, loadOrderToTable);
    } else {
        $.get(`http://localhost:8080/customer/${gCustomerId}/order`, loadOrderToTable)
    }
}

// hàm thu thập dữ liệu để tạo hoặc update album
function getInfoOrder(paramOrder) {
    paramOrder.orderDate = $("#modal-input-order-date").val();
    paramOrder.requiredDate = $("#modal-input-required-date").val();
    paramOrder.shippedDate = $("#modal-input-shipped-date").val();
    paramOrder.status = $("#modal-input-status").val();
    paramOrder.comments = $("#modal-input-comment").val();
}

// hàm validate dữ liệu thu thập được
function validateInfoOrder(paramOrder) {
    if (gCustomerId == 0 && gFormMode == gFORM_MODE_INSERT) {
        alert("Hãy chọn 1 customer để tạo order");
        return false;
    }
    if (paramOrder.orderDate == "") {
        alert("không được để trống Order date");
        return false;
    }
    if (paramOrder.requiredDate == "") {
        alert("không được để trống required date");
        return false;
    }
    if (paramOrder.shippedDate == "") {
        alert("không được để trống shipped date");
        return false;
    }
    if (paramOrder.status == "") {
        alert("không được để trống status");
        return false;
    }
    return true;
}

// hàm xử lý create or update payment
function handleInfoOrder(paramOrder) {
    if (gFormMode === gFORM_MODE_INSERT) {
        callApiCreateOrder(paramOrder);
    } else if (gFormMode === gFORM_MODE_UPDATE) {
        callApiUpdateOrder(paramOrder);
    }
}

// hàm hiển thị thông tin album ra font-end
function showInfoOrderToModal(paramOrderDetail) {
    $("#modal-input-order-date").val(paramOrderDetail.orderDate.split('T')[0]);
    $("#modal-input-required-date").val(paramOrderDetail.requiredDate.split('T')[0]);
    $("#modal-input-shipped-date").val(paramOrderDetail.shippedDate.split('T')[0]);
    $("#modal-input-status").val(paramOrderDetail.status);
    $("#modal-input-comment").val(paramOrderDetail.comments);
    $("#modal-info-order").modal("show");
}

// hàm xử lý chuyển đổi kiểu dữ liệu date
function changeFormatDate(paramDate) {
    var formatDate = paramDate.toISOString().split('T')[0];
    console.log(formatDate);
    var changeFormat = null;
    if (paramDate != null) {
        var date = formatDate.split('-');
        changeFormat = date[2] + "-" + date[1] + "-" + date[0];
    } else { changeFormat = paramDate; }
    
    return changeFormat;
}

/**
   * Hàm reset form về lại mặc định
   * Input: form còn chứa dữ liệu
   * Output: form rỗng
   */
function resetFormToStart() {
    gFormMode = gFORM_MODE_NORMAL;
    gOrderId = 0;
    $("#modal-input-order-date").val("");
    $("#modal-input-required-date").val("");
    $("#modal-input-shipped-date").val("");
    $("#modal-input-status").val("");
    $("#modal-input-comment").val("");
}

// call api create album
function callApiCreateOrder(paramCustomer) {
    $.ajax({
        url: "http://localhost:8080/customer/" + gCustomerId + "/order",
        type: "POST",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramCustomer),
        success: function (res) {
            console.log(res);
            alert(`Đã tạo thành công order`);
            $("#modal-info-order").modal("hide");
            resetFormToStart();
            $.get(`http://localhost:8080/customer/${gCustomerId}/order`, loadOrderToTable);
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api update dữ liệu
function callApiUpdateOrder(paramPayment) {
    $.ajax({
        url: "http://localhost:8080/order/" + gOrderId,
        type: "PUT",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramPayment),
        success: function (res) {
            console.log(res);
            alert(`Đã update thành công order`);
            $("#modal-info-order").modal("hide");
            resetFormToStart();
            if (gCustomerId == 0) {
                $.get(`http://localhost:8080/order`, loadOrderToTable);
            } else {
                $.get(`http://localhost:8080/customer/${gCustomerId}/order`, loadOrderToTable)
            }
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api delete all album
function callApiDeleteAllOrder() {
    $.ajax({
        url: "http://localhost:8080/order",
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete All order");
            $("#modal-delete-all-order").modal("hide");
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// call api delete album by id
function callApiDeleteOrderById() {
    $.ajax({
        url: "http://localhost:8080/order/" + gOrderId,
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete order");
            $("#modal-delete-order").modal("hide");
            resetFormToStart();
            if (gCustomerId == 0) {
                $.get(`http://localhost:8080/order`, loadOrderToTable);
            } else {
                $.get(`http://localhost:8080/customer/${gCustomerId}/order`, loadOrderToTable)
            }
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}