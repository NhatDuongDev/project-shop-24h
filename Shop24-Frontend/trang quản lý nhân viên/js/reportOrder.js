/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// khai báo biến chứa khi tha đổi
var gTypeReport = 1;
var gArrayAmmount = [];
var gArrayCol = [];

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$('#reservation').daterangepicker();

onPageLoading();

$('#reservation').on('change', changeDateRange);
$('#select-type-report').on('change', changeTypeReportOrder);


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
	$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataOrderDay/excel")
	$('#reservation').val('');
	callApiGetDataByDate('reportOrderDay');
}

// hàm xử lý khi thay đổi ngày hiển thị
function changeDateRange() {
	var vChangeDay = $('#reservation').val();
	console.log(vChangeDay);
	var vConditionDay = vChangeDay.split(' - ');

	if (gTypeReport == 1) {
		var api = 'reportOrderDay/' + changeFormatDate(vConditionDay[0]) + '/' + changeFormatDate(vConditionDay[1]);
		callApiGetDataByDate(api);
	} else if (gTypeReport == 2) {
		var api = 'reportOrderWeek/' + changeFormatDate(vConditionDay[0]) + '/' + changeFormatDate(vConditionDay[1]);
		callApiGetDataByDate(api);
	} else if (gTypeReport == 3) {
		var api = 'reportOrderMonth/' + changeFormatDate(vConditionDay[0]) + '/' + changeFormatDate(vConditionDay[1]);
		callApiGetDataByDate(api);
	}
}

// hàm xử lý khi thay đổi type hiển thị
function changeTypeReportOrder() {
	gTypeReport = $('#select-type-report').val();
	console.log(gTypeReport);

	if (gTypeReport == 1) {
		$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataOrderDay/excel")
		var api = 'reportOrderDay';
		callApiGetDataByDate(api);
		$('#reservation').val('');
	} else if (gTypeReport == 2) {
		$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataOrderWeek/excel")
		var api = 'reportOrderWeek';
		callApiGetDataByDate(api);
		$('#reservation').val('');
	} else if (gTypeReport == 3) {
		$("#btn-download-excel").attr("href", "http://localhost:8080/export/dataOrderMonth/excel")
		var api = 'reportOrderMonth';
		callApiGetDataByDate(api);
		$('#reservation').val('');
	}
}



/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm load dữ liệu lên line chart
function loadDataToLineChart(paramArray) {
	for (var vI = 0; vI < paramArray.length; vI++) {
		gArrayCol.push(paramArray[vI].name);
		gArrayAmmount.push(paramArray[vI].total);
	}

	var areaChartData = {
		labels: gArrayCol,
		datasets: [
			{
				label: 'Tổng tiền order',
				backgroundColor: 'rgba(60,141,188,0.9)',
				borderColor: 'rgba(60,141,188,0.8)',
				pointRadius: false,
				pointColor: '#3b8bba',
				pointStrokeColor: 'rgba(60,141,188,1)',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(60,141,188,1)',
				data: gArrayAmmount
			}
		]
	};

	var areaChartOptions = {
		maintainAspectRatio: false,
		responsive: true
	};

	//-------------
	//- LINE CHART -
	//--------------
	var lineChartCanvas = $('#lineChart').get(0).getContext('2d');
	var lineChartOptions = $.extend(true, {}, areaChartOptions);
	var lineChartData = $.extend(true, {}, areaChartData);
	lineChartData.datasets[0].fill = false;
	lineChartOptions.datasetFill = false;

	var lineChart = new Chart(lineChartCanvas, {
		type: 'line',
		data: lineChartData,
		options: lineChartOptions
	});
}

// hàm xử lý chuyển đổi kiểu dữ liệu date
function changeFormatDate(paramDate) {
	var changeFormat = null;
	if (paramDate != null) {
		var date = paramDate.split('/');
		changeFormat = date[2] + '-' + date[0] + '-' + date[1];
	} else {
		changeFormat = paramDate;
	}

	return changeFormat;
}

// hàm gọi api lấy dữ liệu lọc theo ngày
function callApiGetDataByDate(paramAPI) {
	$.ajax({
		url: 'http://localhost:8080/' + paramAPI,
		method: 'GET',
		async: false,
		success: function(pObjRes) {
			gArrayAmmount = [];
			gArrayCol = [];
			loadDataToLineChart(pObjRes);
		},
		error: function(pXhrObj) {
			console.log(pXhrObj);
		}
	});
}
