/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khởi tạo DataTable, gán data và mapping các columns
var gProductLineTable = $('#product-line-table').DataTable({
    columns: [
        { data: 'id' },
        { data: 'productLine' },
        { data: 'description' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 3,
            defaultContent: `
        <a href="#" class="fa-lg mr-2"><i class="fas fa-edit text-primary"></i></a>| 
		<a href="#" class="fa-lg ml-2"><i class="fas fa-trash text-danger"></i></a>`,
        },
    ],
});

// biến chưa dữ liệu info album
var gInfoProductLine = {
    productLine: "",
    description: "",
}

// biến chứa data album
var gCustomer = [];

// biến chứa id album
var gIdProductLine = 0;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

$("#create-product-line").on("click", onBtnCreateProductLineClick);
$("#update-product-line").on("click", onBtnUpdateProductLineClick);
$("#delete-all-product-line").on("click", onBtnDeleteAllProductLineClick);
$("#product-line-table tbody").on("click",".fa-edit", function() {
    onIconEditClick(this);
});
$("#product-line-table tbody").on("click",".fa-trash", function() {
    onIconDeleteClick(this);
});

$("#modal-btn-delete-all-product-line").on("click", callApiDeleteAllproductLine);
$("#modal-btn-delete-product-line").on("click", callApiDeleteProductLineById);


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.get(`http://localhost:8080/productline`, loadProductLineToTable);
}

// hàm xử lý khi nhấn nút create album
function onBtnCreateProductLineClick() {
    // 1: thu thập dữ liệu
    getInfoProductLine();
    // 2: validate dữ liệu
    var vIsValidate = validateInfoProductLine();
    if (vIsValidate) {
        // 3: xử lý nghiệp vụ
        callApiCreateProductLine(gInfoProductLine);
    }
}

// hàm xử lý khi nhấn vào icon edit
function onIconEditClick(paramIconEdit) {
    gIdProductLine = $(paramIconEdit).parents("tr").find("td:first").html();
    $.get(`http://localhost:8080/productline/${gIdProductLine}`, showInfoProductLine);
    $("#create-product-line").prop("disabled",  true);
    $("#delete-all-product-line").prop("disabled",  true);
}

// hàm xử lý khi nhấn vào icon delete
function onIconDeleteClick(paramIconDelete) {
    gIdProductLine = $(paramIconDelete).parents("tr").find("td:first").html();
    $("#modal-delete-product-line").modal("show");
}

// hàm xử lý khi nhấn nút update album
function onBtnUpdateProductLineClick() {
    // 1: thu thập dữ liệu
    getInfoProductLine();
    // 2: validate dữ liệu
    var vIsValidate = validateInfoProductLine();
    if (vIsValidate) {
        // 3: xử lý nghiệp vụ
        callApiUpdateProductLine(gInfoProductLine);
    }
}

// hàm xử lý khi nhấn nut delete all album
function onBtnDeleteAllProductLineClick() {
    $("#modal-delete-all-product-line").modal("show");
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load dữ liệu data lên album table
function loadProductLineToTable(paramProductLine) {
    gProductLineTable.clear();
    gProductLineTable.rows.add(paramProductLine);
    gProductLineTable.draw();
}

// hàm thu thập dữ liệu để tạo hoặc update album
function getInfoProductLine() {
    gInfoProductLine.productLine = $("#input-product-line").val();
    gInfoProductLine.description = $("#input-description").val();
}

// hàm validate dữ liệu thu thập được
function validateInfoProductLine() {
    if (gInfoProductLine.productLine == "") {
        alert("không được để trống product line");
        return false;
    }
    if (gInfoProductLine.description == "") {
        alert("không được để trống description");
        return false;
    }
    return true;
}

// hàm hiển thị thông tin Product line ra font-end
function showInfoProductLine(paramProductLine) {
    $("#input-product-line").val(paramProductLine.productLine);
    $("#input-description").val(paramProductLine.description);
}

// call api create album
function callApiCreateProductLine(paramProductLine) {
    $.ajax({
        url: "http://localhost:8080/productline",
        type: "POST",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramProductLine),
        success: function (res) {
            alert(`Đã tạo thành công product line`);
            location.reload();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api update dữ liệu
function callApiUpdateProductLine(paramProductLine) {
    $.ajax({
        url: "http://localhost:8080/productline/" + gIdProductLine,
        type: "PUT",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramProductLine),
        success: function (res) {
            alert(`Đã update thành công product line`);
            location.reload();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api delete all album
function callApiDeleteAllproductLine() {
    $.ajax({
        url: "http://localhost:8080/productline",
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete All product line");
            $("#modal-delete-all-product-line").modal("hide");
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// call api delete album by id
function callApiDeleteProductLineById() {
    $.ajax({
        url: "http://localhost:8080/productline/" + gIdProductLine,
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete product line");
            $("#modal-delete-product-line").modal("hide");
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}