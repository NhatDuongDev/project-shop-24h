/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Khởi tạo DataTable, gán data và mapping các columns
var gOrderDetailTable = $('#order-detail-table').DataTable({
    columns: [
        { data: 'id' },
        { data: 'quantityOrder' },
        { data: 'priceEach' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 3,
            defaultContent: `
        <a href="#" class="fa-lg mr-2"><i class="fas fa-edit text-primary"></i></a>| 
		<a href="#" class="fa-lg ml-2"><i class="fas fa-trash text-danger"></i></a>`,
        },
    ],
});

// Khai báo biến hằng lưu trữ trạng thái form
const gFORM_MODE_NORMAL = 'Normal';
const gFORM_MODE_INSERT = 'Insert';
const gFORM_MODE_UPDATE = 'Update';
const gFORM_MODE_DELETE = 'Delete';
// Biến toàn cục lưu trạng thái form, mặc định NORMAL
var gFormMode = gFORM_MODE_NORMAL;

// biến chứa id
var gProductId = 0;
var gOrderId = 0;
var gOrderDetailId = 0;

// biến chứa data album
var gCustomer = [];



/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

onPageLoading();

$("#select-order").change(onGetOrderChange);
$("#select-product").change(onGetProductChange);

$("#create-order-detail").on("click", onBtnCreateOrderDetailClick);
$("#btn-modal-save-order-detail").on("click", onModalBtnSaveOrderDetailClick);
$("#delete-all-order-detail").on("click", onBtnDeleteAllOrderDetailClick);
$("#order-detail-table tbody").on("click", ".fa-edit", function () {
    onIconEditClick(this);
});
$("#order-detail-table tbody").on("click", ".fa-trash", function () {
    onIconDeleteClick(this);
});

$("#modal-btn-delete-all-order-detail").on("click", callApiDeleteAllOrderDetail);
$("#modal-btn-delete-order-detail").on("click", callApiDeleteOrderDetailById);

// reset form khi hidden modal
$('#modal-info-order-detail').on('hidden.bs.modal', resetFormToStart);
$('#modal-delete-order-detail').on('hidden.bs.modal', resetFormToStart);


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.get(`http://localhost:8080/product`, showProductToSelectElement);
    $.get(`http://localhost:8080/order`, showOrderToSelectElement);
    $.get(`http://localhost:8080/orderdetail`, loadOrderDetailToTable);
}

// hàm xử lý khi nhấn nút create album
function onBtnCreateOrderDetailClick() {
    gFormMode = gFORM_MODE_INSERT;
    $("#modal-input-quantity-order").val("");
    $("#modal-input-price-each").val("");
    $("#modal-info-order-detail").modal("show");
}

// hàm xử lý khi nhấn vào icon edit
function onIconEditClick(paramIconEdit) {
    gFormMode = gFORM_MODE_UPDATE;
    gOrderDetailId = $(paramIconEdit).parents("tr").find("td:first").html();
    $.get(`http://localhost:8080/orderdetail/${gOrderDetailId}`, showInfoOrderDetailToModal);
}

// hàm xử lý khi nhấn vào icon delete
function onIconDeleteClick(paramIconDelete) {
    gFormMode = gFORM_MODE_DELETE;
    gOrderDetailId = $(paramIconDelete).parents("tr").find("td:first").html();
    $("#modal-delete-order-detail").modal("show");
}

// hàm xử lý khi nhấn nút save trên modal
function onModalBtnSaveOrderDetailClick() {
    // biến chưa dữ liệu info payment
    var vInfoOrderDetail = {
        quantityOrder: 0,
        priceEach: 0,
    }
    // 1: thu thập dữ liệu
    getInfoOrderDetail(vInfoOrderDetail);
    // 2: validate dữ liệu
    var vIsValidate = validateInfoOrderDetail(vInfoOrderDetail);
    if (vIsValidate) {
        // 3: xử lý nghiệp vụ
        handleInfoOrderDetail(vInfoOrderDetail);
    }
}

// hàm xử lý khi nhấn nut delete all album
function onBtnDeleteAllOrderDetailClick() {
    $("#modal-delete-all-order-detail").modal("show");
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load dữ liệu data lên album table
function loadOrderDetailToTable(paramProduct) {
    console.log(paramProduct);
    gOrderDetailTable.clear();
    gOrderDetailTable.rows.add(paramProduct);
    gOrderDetailTable.draw();
}

// show data product vào select element
function showProductToSelectElement(paramProduct) {
    var vProductSelectElement = $("#select-product");
    for (var vI = 0; vI < paramProduct.length; vI++) {
        $("<option/>", { text: paramProduct[vI].productCode, value: paramProduct[vI].id }).appendTo(vProductSelectElement);
    }
}

// show data order vào select element
function showOrderToSelectElement(paramOrder) {
    var vOrderSelectElement = $("#select-order");
    for (var vI = 0; vI < paramOrder.length; vI++) {
        $("<option/>", { text: paramOrder[vI].id, value: paramOrder[vI].id }).appendTo(vOrderSelectElement);
    }
}

// hàm xử lý khi thay đổi product sẽ load ra table payment tương ứng
function onGetProductChange(event) {
    gProductId = event.target.value;
    console.log(gProductId);
    if (gProductId != 0 && gOrderId == 0) {
        $.get(`http://localhost:8080/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
    } else if (gProductId != 0 && gOrderId != 0) {
        $.get(`http://localhost:8080/order/${gOrderId}/product/${gProductId}/orderdetail`, loadOrderDetailToTable)
    } else if (gProductId == 0 && gOrderId != 0) {
        $.get(`http://localhost:8080/order/${gOrderId}/orderdetail`, loadOrderDetailToTable);
    }
}

// hàm xử lý khi thay đổi order sẽ load ra table payment tương ứng
function onGetOrderChange(event) {
    gOrderId = event.target.value;
    console.log(gOrderId);
    if (gProductId == 0 && gOrderId != 0) {
        $.get(`http://localhost:8080/order/${gOrderId}/orderdetail`, loadOrderDetailToTable);
    } else if (gProductId != 0 && gOrderId != 0) {
        $.get(`http://localhost:8080/order/${gOrderId}/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
    } else if (gProductId != 0 && gOrderId == 0) {
        $.get(`http://localhost:8080/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
    }
}

// hàm thu thập dữ liệu để tạo hoặc update album
function getInfoOrderDetail(paramOrderDetail) {
    paramOrderDetail.quantityOrder = $("#modal-input-quantity-order").val();
    paramOrderDetail.priceEach = $("#modal-input-price-each").val();
}

// hàm validate dữ liệu thu thập được
function validateInfoOrderDetail(paramOrderDetail) {
    if (gProductId == 0 && gOrderId == 0 && gFormMode == gFORM_MODE_INSERT) {
        alert("Hãy chọn 1 product va 1 order để tạo order detail");
        return false;
    }
    if (gProductId == 0 && gFormMode == gFORM_MODE_INSERT) {
        alert("Hãy chọn 1 product để tạo order detail");
        return false;
    }
    if (gOrderId == 0 && gFormMode == gFORM_MODE_INSERT) {
        alert("Hãy chọn 1 order để tạo order detail");
        return false;
    }
    if (paramOrderDetail.quantityOrder == "") {
        alert("không được để trống Quantity Order");
        return false;
    }
    if (paramOrderDetail.priceEach == "") {
        alert("không được để trống Price Each");
        return false;
    }
    return true;
}

// hàm xử lý create or update payment
function handleInfoOrderDetail(paramOrderDetail) {
    if (gFormMode === gFORM_MODE_INSERT) {
        callApiCreateOrderDetail(paramOrderDetail);
    } else if (gFormMode === gFORM_MODE_UPDATE) {
        callApiUpdateOrderDetail(paramOrderDetail);
    }
}

// hàm hiển thị thông tin album ra font-end
function showInfoOrderDetailToModal(paramProductDetail) {
    $("#modal-input-quantity-order").val(paramProductDetail.quantityOrder);
    $("#modal-input-price-each").val(paramProductDetail.priceEach);
    $("#modal-info-order-detail").modal("show");
}

/**
   * Hàm reset form về lại mặc định
   * Input: form còn chứa dữ liệu
   * Output: form rỗng
   */
function resetFormToStart() {
    gFormMode = gFORM_MODE_NORMAL;
    gOrderDetailId = 0;
    $("#modal-input-quantity-order").val("");
    $("#modal-input-price-each").val("");
}

// call api create album
function callApiCreateOrderDetail(paramOrderDetail) {
    $.ajax({
        url: "http://localhost:8080/order/" + gOrderId + "/product/" + gProductId + "/orderdetail",
        type: "POST",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramOrderDetail),
        success: function (res) {
            console.log(res);
            alert(`Đã tạo thành công Order Detail`);
            $("#modal-info-order-detail").modal("hide");
            resetFormToStart();
            $.get(`http://localhost:8080/order/${gOrderId}/product/${gProductId}/orderdetail`, loadOrderDetailToTable)
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api update dữ liệu
function callApiUpdateOrderDetail(paramOrderDetail) {
    $.ajax({
        url: "http://localhost:8080/orderdetail/" + gOrderDetailId,
        type: "PUT",
        contentType: 'application/json', // added data type
        data: JSON.stringify(paramOrderDetail),
        success: function (res) {
            console.log(res);
            alert(`Đã update thành công Order Detail`);
            $("#modal-info-order-detail").modal("hide");
            resetFormToStart();
            if (gProductId == 0 && gOrderId != 0) {
                $.get(`http://localhost:8080/order/${gOrderId}/orderdetail`, loadOrderDetailToTable);
            } else if (gProductId != 0 && gOrderId != 0) {
                $.get(`http://localhost:8080/order/${gOrderId}/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
            } else if (gProductId != 0 && gOrderId == 0) {
                $.get(`http://localhost:8080/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
            }
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}

// call api delete all album
function callApiDeleteAllOrderDetail() {
    $.ajax({
        url: "http://localhost:8080/orderdetail",
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete All Order Detail");
            $("#modal-delete-all-order-detail").modal("hide");
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// call api delete album by id
function callApiDeleteOrderDetailById() {
    $.ajax({
        url: "http://localhost:8080/orderdetail/" + gOrderDetailId,
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("Successfully Delete Order Detail");
            $("#modal-delete-order-detail").modal("hide");
            resetFormToStart();
            if (gProductId == 0 && gOrderId != 0) {
                $.get(`http://localhost:8080/order/${gOrderId}/orderdetail`, loadOrderDetailToTable);
            } else if (gProductId != 0 && gOrderId != 0) {
                $.get(`http://localhost:8080/order/${gOrderId}/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
            } else if (gProductId != 0 && gOrderId == 0) {
                $.get(`http://localhost:8080/product/${gProductId}/orderdetail`, loadOrderDetailToTable);
            }
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}