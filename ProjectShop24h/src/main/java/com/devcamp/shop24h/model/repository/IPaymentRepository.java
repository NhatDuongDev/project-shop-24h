package com.devcamp.shop24h.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.CPayment;

public interface IPaymentRepository extends JpaRepository<CPayment, Long> {
	List<CPayment> findByCustomerId(long id);
}
