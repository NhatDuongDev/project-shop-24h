package com.devcamp.shop24h.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "images")
public class CImage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "link_image")
	@NotEmpty(message = "phải có link image")
	private String linkImage;
	
	@Column(name = "image_description")
	private String imageDescription;
	
	@ManyToOne
	@JsonIgnore
	private CProduct product;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLinkImage() {
		return linkImage;
	}
	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
	}
	public String getImageDescription() {
		return imageDescription;
	}
	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}
	public CProduct getProduct() {
		return product;
	}
	public void setProduct(CProduct product) {
		this.product = product;
	}
	
}
