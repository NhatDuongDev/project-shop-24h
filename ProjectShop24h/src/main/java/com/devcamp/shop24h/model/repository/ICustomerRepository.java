package com.devcamp.shop24h.model.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
	Optional<CCustomer> findByPhoneNumber(String phoneNumber);
	
	@Query(value = "SELECT `id`, `last_name`, `first_name` FROM `customers` WHERE 1", nativeQuery = true)
	ArrayList<Object> getCustomerSelect();
	
	@Query(value = "SELECT Date(orders.order_date) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details WHERE orders.id = order_details.order_id GROUP BY Date(order_date)", nativeQuery = true)
	ArrayList<testrepository> getReportOrderDay();

	@Query(value = "SELECT Date(orders.order_date) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details WHERE orders.id = order_details.order_id AND (orders.order_date BETWEEN :day1 AND :day2) GROUP BY Date(order_date)", nativeQuery = true)
	ArrayList<testrepository> getReportOrderDayCondition(@Param("day1") String day1, @Param("day2") String day2);
	
	@Query(value = "SELECT CONCAT(FROM_DAYS(TO_DAYS(order_date)-MOD(TO_DAYS(order_date)-2,7)), \" -> \", DATE_ADD(FROM_DAYS(TO_DAYS(order_date)-MOD(TO_DAYS(order_date)-2,7)), INTERVAL 7 DAY)) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details WHERE orders.id = order_details.order_id GROUP BY FROM_DAYS(TO_DAYS(order_date)-MOD(TO_DAYS(order_date)-2,7))", nativeQuery = true)
	ArrayList<testrepository> getReportOrderWeek();
	
	@Query(value = "SELECT CONCAT(FROM_DAYS(TO_DAYS(order_date)-MOD(TO_DAYS(order_date)-2,7)), \" -> \", DATE_ADD(FROM_DAYS(TO_DAYS(order_date)-MOD(TO_DAYS(order_date)-2,7)), INTERVAL 7 DAY)) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details WHERE orders.id = order_details.order_id AND (orders.order_date BETWEEN :day1 AND :day2) GROUP BY FROM_DAYS(TO_DAYS(order_date)-MOD(TO_DAYS(order_date)-2,7))", nativeQuery = true)
	ArrayList<testrepository> getReportOrderWeekCondition(@Param("day1") String day1, @Param("day2") String day2);
	
	@Query(value = "SELECT MONTH(orders.order_date) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details WHERE orders.id = order_details.order_id GROUP BY MONTH(order_date)", nativeQuery = true)
	ArrayList<testrepository> getReportOrderMonth();
	
	@Query(value = "SELECT MONTH(orders.order_date) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details WHERE orders.id = order_details.order_id AND (orders.order_date BETWEEN :day1 AND :day2) GROUP BY MONTH(order_date)", nativeQuery = true)
	ArrayList<testrepository> getReportOrderMonthCondition(@Param("day1") String day1, @Param("day2") String day2);
	
	
	@Query(value = "SELECT CONCAT(customers.id, \" \", customers.first_name, \" \", customers.last_name) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details, customers WHERE orders.id = order_details.order_id AND customers.id = orders.customer_id GROUP BY orders.customer_id HAVING total >= :number", nativeQuery = true)
	ArrayList<testrepository> getReportCustomerAmmountCondition(@Param("number") String number);
	
	@Query(value = "SELECT CONCAT(customers.id, \" \", customers.first_name, \" \", customers.last_name) AS name, SUM(order_details.price_each * order_details.quantity_order) AS total FROM orders, order_details, customers WHERE orders.id = order_details.order_id AND customers.id = orders.customer_id GROUP BY orders.customer_id", nativeQuery = true)
	ArrayList<testrepository> getReportCustomerAmmount();
	
	@Query(value = "SELECT CONCAT(customers.id, \" \", customers.first_name, \" \", customers.last_name) AS name, COUNT(orders.id) AS total FROM orders, customers WHERE customers.id = orders.customer_id GROUP BY orders.customer_id HAVING total >= :number", nativeQuery = true)
	ArrayList<testrepository> getReportCustomerOrderCondition(@Param("number") String number);
	
	@Query(value = "SELECT CONCAT(customers.id, \" \", customers.first_name, \" \", customers.last_name) AS name, COUNT(orders.id) AS total FROM orders, customers WHERE customers.id = orders.customer_id GROUP BY orders.customer_id", nativeQuery = true)
	ArrayList<testrepository> getReportCustomerOrder();
}
