package com.devcamp.shop24h.model;

import java.math.BigDecimal;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_details")
public class COrderDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JsonIgnore
	private COrder order;
	
	@ManyToOne
	@JsonIgnore
	private CProduct product;
	
	@Column(name = "quantity_order")
	private int quantityOrder;
	@Column(name = "price_each")
	private BigDecimal priceEach;
	
	@Transient
	private long producId;
	
	/*
	@Transient
	private long orderName;
	
	public long getOrderName() {
		return getOrder().getId();
	}
	public void setOrderName(long orderName) {
		this.orderName = orderName;
	}
	*/
	
	
	public void setProducId(long producId) {
		this.producId = producId;
	}
	
	public long getProducId() {
		return producId;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public COrder getOrder() {
		return order;
	}
	public void setOrder(COrder order) {
		this.order = order;
	}
	
	public CProduct getProduct() {
		return product;
	}
	public void setProduct(CProduct product) {
		this.product = product;
	}
	public int getQuantityOrder() {
		return quantityOrder;
	}
	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}
	public BigDecimal getPriceEach() {
		return priceEach;
	}
	public void setPriceEach(BigDecimal priceEach) {
		this.priceEach = priceEach;
	}

	
}
