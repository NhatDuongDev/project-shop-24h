package com.devcamp.shop24h.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.COrderDetail;

public interface IOrderDetailRepository extends JpaRepository<COrderDetail, Long> {
	List<COrderDetail> findByOrderId(long Id);
	List<COrderDetail> findByProductId(long Id);
	List<COrderDetail> findByOrderIdAndProductId(long orderId, long productId);
}
