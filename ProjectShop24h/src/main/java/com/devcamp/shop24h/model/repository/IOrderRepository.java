package com.devcamp.shop24h.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
	
	List<COrder> findByCustomerId(long id);
}
