package com.devcamp.shop24h.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "payments")
public class CPayment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JsonIgnore
	private CCustomer customer;
	
	@Column(name = "check_number")
	private String checkNumber;
	@Column(name = "payment_date")
	private Date paymentDate;
	private BigDecimal ammount;
	/*
	@Transient
	private String customerFullname;
	
	public String getCustomerFullname() {
		return getCustomer().getLastname() + " " + getCustomer().getFirstname();
	}
	public void setCustomerFullname(String customerFullname) {
		this.customerFullname = customerFullname;
	}*/
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public CCustomer getCustomer() {
		return customer;
	}
	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public BigDecimal getAmmount() {
		return ammount;
	}
	public void setAmmount(BigDecimal ammount) {
		this.ammount = ammount;
	}
	
	
}	
