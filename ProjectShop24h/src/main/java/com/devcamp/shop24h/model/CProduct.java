package com.devcamp.shop24h.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
public class CProduct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "product_code", unique = true)
	@NotNull(message = "phải có product code")
	private String productCode;
	
	@Column(name = "product_name")
	@NotNull(message = "phải có product name")
	private String productName;
	
	@Column(name = "product_description")
	private String productDescription;
	
	@ManyToOne
	@JsonIgnore
	private CProductLine productLine;
	
	@Column(name = "product_image")
	private String productImage;
	
	@Column(name = "product_vendor")
	private String productVendor;
	
	@Column(name = "quantity_in_stock")
	private int quantityInStock;
	
	@Column(name = "buy_price")
	private BigDecimal buyPrice;
	
	@Column(name = "sell_price")
	private BigDecimal sellPrice;
	
	@Column(name = "product_weight")
	private long productWeight;
	
	@OneToMany(targetEntity = COrderDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
	private List<COrderDetail> orderDetails;
	
	@OneToMany(targetEntity = CImage.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")	
	private List<CImage> images;
	
	public CProduct() {
		super();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public String getProductVendor() {
		return productVendor;
	}
	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}
	public int getQuantityInStock() {
		return quantityInStock;
	}
	public void setQuantityInStock(int quantityInStock) {
		this.quantityInStock = quantityInStock;
	}
	public BigDecimal getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}
	@JsonIgnore
	public List<COrderDetail> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<COrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
	public CProductLine getProductLine() {
		return productLine;
	}
	public void setProductLine(CProductLine productLine) {
		this.productLine = productLine;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public long getProductWeight() {
		return productWeight;
	}
	public void setProductWeight(long productWeight) {
		this.productWeight = productWeight;
	}

	public List<CImage> getImages() {
		return images;
	}

	public void setImages(List<CImage> images) {
		this.images = images;
	}
	
	
}
