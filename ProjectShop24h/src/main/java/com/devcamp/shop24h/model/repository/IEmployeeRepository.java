package com.devcamp.shop24h.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.CEmployee;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Long> {

}
