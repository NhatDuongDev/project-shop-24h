package com.devcamp.shop24h.model.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.CProduct;


public interface IProductRepository extends JpaRepository<CProduct, Long> {
	@Query(value = "SELECT products.*, product_lines.productLine FROM products, product_lines  WHERE products.productLineId = product_lines.id", nativeQuery = true)
	List<CProduct> getAllProduct();
	
	List<CProduct> findByProductLineId(long id);
	
	@Query(value = "SELECT * FROM `products` WHERE 1", nativeQuery = true)
	List<CProduct> getProductByPage(Pageable pageable);
	
	@Query(value = "SELECT * FROM products WHERE product_line_id like :id", nativeQuery = true)
	List<CProduct> getPageProductByProductLine(@Param("id") int id, Pageable pageable);
}

