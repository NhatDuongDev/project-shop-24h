package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "customers")
public class CCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "last_name")
	@NotNull(message = "phải có lastname")
	private String lastname;
	
	@Column(name = "first_name")
	@NotNull(message = "phải có firstname")
	private String firstname;
	
	@Column(name = "phone_number", unique = true)
	@NotNull(message = "phải có phone number")
	@Size(min = 3, message = "phone number có ít nhất 3 ký tự")
	private String phoneNumber;
	
	@NotNull(message = "phải có address")
	private String address;
	
	@NotNull(message = "phải có city")
	private String city;
	
	private String state;
	
	@Column(name = "postal_code")
	private String postalCode;
	
	@NotNull(message = "phải có country")
	private String country;
	
	@Column(name = "sales_rep_employee_number")
	private int salesRepEmployeeNumber;
	
	@Column(name = "credit_limit")
	private int creditLimit;
	
	@OneToMany(targetEntity = COrder.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private List<COrder> orders;
	
	@OneToMany(targetEntity = CPayment.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private List<CPayment> payment;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}
	public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}
	public int getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public List<COrder> getOrders() {
		return orders;
	}
	public void setOrders(List<COrder> orders) {
		this.orders = orders;
	}

	public List<CPayment> getPayment() {
		return payment;
	}
	public void setPayment(List<CPayment> payment) {
		this.payment = payment;
	}
	
}
