package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_lines")
public class CProductLine {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "product_line", unique = true)
	private String productLine;
	@Column(name = "description")
	private String description;
	
	@OneToMany(targetEntity = CProduct.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_line_id")
	private List<CProduct> product;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProductLine() {
		return productLine;
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@JsonIgnore
	public List<CProduct> getProduct() {
		return product;
	}
	public void setProduct(List<CProduct> product) {
		this.product = product;
	}
	
	
}
