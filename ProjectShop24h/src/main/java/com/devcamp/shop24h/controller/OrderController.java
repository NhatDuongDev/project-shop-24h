package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.shop24h.model.CCustomer;
import com.devcamp.shop24h.model.COrder;
import com.devcamp.shop24h.model.repository.ICustomerRepository;
import com.devcamp.shop24h.model.repository.IOrderRepository;

@CrossOrigin
@Controller
public class OrderController {
	@Autowired
	IOrderRepository orderRepository;
	@Autowired
	ICustomerRepository customerRepository;

	@GetMapping("/order")
	public ResponseEntity<List<COrder>> getAllOrder() {
		try {
			List<COrder> pOrder = new ArrayList<>();
			orderRepository.findAll().forEach(pOrder::add);
			return new ResponseEntity<>(pOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customer/{customerId}/order")
	public ResponseEntity<List<COrder>> getOrderByCustomerId(@PathVariable Long customerId) {
		try {
			List<COrder> pOrder = orderRepository.findByCustomerId(customerId);
			return new ResponseEntity<>(pOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/order/{id}")
	public ResponseEntity<COrder> getOrderById(@PathVariable Long id) {
		try {
			COrder order = orderRepository.findById(id).get();
			return new ResponseEntity<>(order, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/customer/{id}/order")
	public ResponseEntity<Object> createOrder(@PathVariable Long id, @RequestBody COrder order) {
		try {
			Optional<CCustomer> customertData = customerRepository.findById(id);
			if (customertData.isPresent()) {
				COrder createOrder = new COrder();
				createOrder.setOrderDate(order.getOrderDate());
				createOrder.setRequiredDate(order.getRequiredDate());
				createOrder.setShippedDate(order.getShippedDate());
				createOrder.setStatus(order.getStatus());
				createOrder.setComments(order.getComments());
				createOrder.setCustomer(customertData.get());

				COrder saveOrder = orderRepository.save(createOrder);
				return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Order: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/order/{id}")
	public ResponseEntity<Object> updateOrder(@PathVariable Long id, @RequestBody COrder order) {
		try {
			Optional<COrder> orderData = orderRepository.findById(id);
			if (orderData.isPresent()) {
				COrder updateOrder = orderData.get();
				updateOrder.setOrderDate(order.getOrderDate());
				updateOrder.setRequiredDate(order.getRequiredDate());
				updateOrder.setShippedDate(order.getShippedDate());
				updateOrder.setStatus(order.getStatus());
				updateOrder.setComments(order.getComments());

				COrder saveOrder = orderRepository.save(updateOrder);
				return new ResponseEntity<>(saveOrder, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/order/{id}")
	public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
		try {
			orderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/order")
	public ResponseEntity<Object> deleteAllOrder() {
		try {
			orderRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
