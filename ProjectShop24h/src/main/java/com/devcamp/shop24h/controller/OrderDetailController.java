package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.shop24h.model.COrder;
import com.devcamp.shop24h.model.COrderDetail;
import com.devcamp.shop24h.model.CProduct;
import com.devcamp.shop24h.model.repository.IOrderDetailRepository;
import com.devcamp.shop24h.model.repository.IOrderRepository;
import com.devcamp.shop24h.model.repository.IProductRepository;

@CrossOrigin
@Controller
public class OrderDetailController {
	@Autowired
	IOrderDetailRepository orderDetailRepository;
	@Autowired
	IOrderRepository orderRepository;
	@Autowired
	IProductRepository productRepository;

	@GetMapping("/orderdetail")
	public ResponseEntity<List<COrderDetail>> getAllOrderDetail() {
		try {
			List<COrderDetail> pOrderDetail = new ArrayList<>();
			orderDetailRepository.findAll().forEach(pOrderDetail::add);
			return new ResponseEntity<>(pOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/orderdetail/{id}")
	public ResponseEntity<COrderDetail> getOrderDetailById(@PathVariable Long id) {
		try {
			COrderDetail data = orderDetailRepository.findById(id).get();
			return new ResponseEntity<>(data, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/order/{orderId}/orderdetail")
	public ResponseEntity<List<COrderDetail>> getOrderDetailByOrderId(@PathVariable Long orderId) {
		try {
			List<COrderDetail> pOrderDetail = orderDetailRepository.findByOrderId(orderId);
			return new ResponseEntity<>(pOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/product/{productId}/orderdetail")
	public ResponseEntity<List<COrderDetail>> getOrderDetailByProductId(@PathVariable Long productId) {
		try {
			List<COrderDetail> pOrderDetail = orderDetailRepository.findByProductId(productId);
			return new ResponseEntity<>(pOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/order/{orderId}/product/{productId}/orderdetail")
	public ResponseEntity<List<COrderDetail>> getOrderDetailByOrderIdAndProductId(@PathVariable Long orderId, @PathVariable Long productId) {
		try {
			List<COrderDetail> pOrderDetail = orderDetailRepository.findByOrderIdAndProductId(orderId, productId);
			return new ResponseEntity<>(pOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/order/{orderId}/product/{productId}/orderdetail")
	public ResponseEntity<Object> createOrderDetail(@PathVariable Long orderId, @PathVariable Long productId,
			@RequestBody COrderDetail orderDetail) {
		try {
			Optional<COrder> orderData = orderRepository.findById(orderId);
			Optional<CProduct> productData = productRepository.findById(productId);
			if (orderData.isPresent() && productData.isPresent()) {
				COrderDetail createOrderDetail = new COrderDetail();
				createOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());
				createOrderDetail.setPriceEach(orderDetail.getPriceEach());
				;
				createOrderDetail.setOrder(orderData.get());
				;
				createOrderDetail.setProduct(productData.get());
				;

				COrderDetail saveOrderDetail = orderDetailRepository.save(createOrderDetail);
				return new ResponseEntity<>(saveOrderDetail, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Order Detail: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/orderdetail/{id}")
	public ResponseEntity<Object> updateOrderDetail(@PathVariable Long id, @RequestBody COrderDetail orderDetail) {
		try {
			Optional<COrderDetail> orderDetailData = orderDetailRepository.findById(id);
			if (orderDetailData.isPresent()) {
				COrderDetail updateOrderDetail = orderDetailData.get();
				updateOrderDetail.setQuantityOrder(orderDetail.getQuantityOrder());
				updateOrderDetail.setPriceEach(orderDetail.getPriceEach());
				;

				COrderDetail saveOrderDetail = orderDetailRepository.save(updateOrderDetail);
				return new ResponseEntity<>(saveOrderDetail, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to update specified Order Detail: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/orderdetail/{id}")
	public ResponseEntity<Object> deleteOrderDetailById(@PathVariable Long id) {
		try {
			orderDetailRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/orderdetail")
	public ResponseEntity<Object> deleteAllOrderDetail() {
		try {
			orderDetailRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
