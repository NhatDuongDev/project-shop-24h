package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.shop24h.model.CCustomer;
import com.devcamp.shop24h.model.CPayment;
import com.devcamp.shop24h.model.repository.ICustomerRepository;
import com.devcamp.shop24h.model.repository.IPaymentRepository;


@CrossOrigin
@Controller
public class PaymentController {
	@Autowired
	IPaymentRepository paymentRepository;
	@Autowired
	ICustomerRepository customerRepository;
	
	@GetMapping("/payment")
	public ResponseEntity<List<CPayment>> getAllPayment() {
		try {
			List<CPayment> pPayment = new ArrayList<>();
			paymentRepository.findAll().forEach(pPayment::add);
			return new ResponseEntity<>(pPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customer/{customerId}/payment")
	public ResponseEntity<List<CPayment>>  getPaymentByCustomerId(@PathVariable Long customerId) {
		try {
			List<CPayment> pPayment = paymentRepository.findByCustomerId(customerId);
			return new ResponseEntity<>(pPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/payment/{id}")
	public ResponseEntity<CPayment> getPaymentById(@PathVariable Long id) {
		try {
			CPayment pPayment = paymentRepository.findById(id).get();
			return new ResponseEntity<>(pPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/customer/{id}/payment")
	public ResponseEntity<Object> createPayment(@PathVariable Long id, @RequestBody CPayment payment) {
		try {
			Optional<CCustomer> customertData = customerRepository.findById(id);
			if (customertData.isPresent()) {
				CPayment createPayment = new CPayment();
				createPayment.setCheckNumber(payment.getCheckNumber());
				createPayment.setPaymentDate(payment.getPaymentDate());
				createPayment.setAmmount(payment.getAmmount());
				createPayment.setCustomer(customertData.get());
				
				CPayment savePayment = paymentRepository.save(createPayment);
				return new ResponseEntity<>(savePayment, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Payment: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/payment/{id}")
	public ResponseEntity<Object> updatePayment(@PathVariable Long id, @RequestBody CPayment payment) {
		try {
			Optional<CPayment> paymenttData = paymentRepository.findById(id);
			if (paymenttData.isPresent()) {
				CPayment updatePayment = paymenttData.get();
				updatePayment.setCheckNumber(payment.getCheckNumber());
				updatePayment.setPaymentDate(payment.getPaymentDate());
				updatePayment.setAmmount(payment.getAmmount());
				
				CPayment savePayment = paymentRepository.save(updatePayment);
				return new ResponseEntity<>(savePayment, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Update specified Payment: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/payment/{id}")
	public ResponseEntity<Object> deletePaymentById(@PathVariable Long id) {
		try {
			paymentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/payment")
	public ResponseEntity<Object> deleteAllPayment() {
		try {
			paymentRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
