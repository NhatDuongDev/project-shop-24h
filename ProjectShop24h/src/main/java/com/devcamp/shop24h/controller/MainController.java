package com.devcamp.shop24h.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.devcamp.shop24h.ExcelExporter.OrderExcelExporter;
import com.devcamp.shop24h.model.repository.ICustomerRepository;
import com.devcamp.shop24h.model.repository.testrepository;

@CrossOrigin
@Controller
public class MainController {
	@Autowired
	ICustomerRepository customerRepository;
	
	@GetMapping("/export/dataOrderDay/excel")
	public void exportToExcelOrderDay(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		ArrayList<testrepository> data = customerRepository.getReportOrderDay();
		
		
		OrderExcelExporter orderExcel = new OrderExcelExporter(data);
		orderExcel.export(response);
	}
	
	@GetMapping("/export/dataOrderWeek/excel")
	public void exportToExcelOrderWeek(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		ArrayList<testrepository> data = customerRepository.getReportOrderWeek();
		
		
		OrderExcelExporter orderExcel = new OrderExcelExporter(data);
		orderExcel.export(response);
	}
	
	@GetMapping("/export/dataOrderMonth/excel")
	public void exportToExcelOrderMonth(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		ArrayList<testrepository> data = customerRepository.getReportOrderMonth();
		
		
		OrderExcelExporter orderExcel = new OrderExcelExporter(data);
		orderExcel.export(response);
	}
	
	@GetMapping("/export/dataCustomerAmmount/excel")
	public void exportToExcelCustomerAmmount(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		ArrayList<testrepository> data = customerRepository.getReportCustomerAmmount();
		
		
		OrderExcelExporter orderExcel = new OrderExcelExporter(data);
		orderExcel.export(response);
	}
	
	@GetMapping("/export/dataCustomerOrder/excel")
	public void exportToExcelCustomerOrder(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		ArrayList<testrepository> data = customerRepository.getReportCustomerOrder();
		
		
		OrderExcelExporter orderExcel = new OrderExcelExporter(data);
		orderExcel.export(response);
	}
	
	@GetMapping("/reportOrderDay")
	public ResponseEntity<List<testrepository>> reportOrderDay() {
		try {
			List<testrepository> data = customerRepository.getReportOrderDay();

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportOrderDay/{day1}/{day2}")
	public ResponseEntity<List<testrepository>> reportOrderDayCondition(@PathVariable String day1, @PathVariable String day2) {
		try {
			List<testrepository> data = customerRepository.getReportOrderDayCondition(day1, day2);

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportOrderWeek")
	public ResponseEntity<List<testrepository>> reportOrderWeek() {
		try {
			List<testrepository> data = customerRepository.getReportOrderWeek();

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportOrderWeek/{day1}/{day2}")
	public ResponseEntity<List<testrepository>> reportOrderWeekCondition(@PathVariable String day1, @PathVariable String day2) {
		try {
			List<testrepository> data = customerRepository.getReportOrderWeekCondition(day1, day2);

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportOrderMonth")
	public ResponseEntity<List<testrepository>> reportOrderMonth() {
		try {
			List<testrepository> data = customerRepository.getReportOrderMonth();

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportOrderMonth/{day1}/{day2}")
	public ResponseEntity<List<testrepository>> reportOrderMonthCondition(@PathVariable String day1, @PathVariable String day2) {
		try {
			List<testrepository> data = customerRepository.getReportOrderMonthCondition(day1, day2);

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportCustomerAmmount/{number}")
	public ResponseEntity<List<testrepository>> getReportCustomerAmmountCondition(@PathVariable String number) {
		try {
			List<testrepository> data = customerRepository.getReportCustomerAmmountCondition(number);

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportCustomerAmmount")
	public ResponseEntity<List<testrepository>> getReportCustomerAmmount() {
		try {
			List<testrepository> data = customerRepository.getReportCustomerAmmount();

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportCustomerOrder/{number}")
	public ResponseEntity<List<testrepository>> getReportCustomerOrderCondition(@PathVariable String number) {
		try {
			List<testrepository> data = customerRepository.getReportCustomerOrderCondition(number);

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/reportCustomerOrder")
	public ResponseEntity<List<testrepository>> getReportCustomerOrder() {
		try {
			List<testrepository> data = customerRepository.getReportCustomerOrder();

			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
