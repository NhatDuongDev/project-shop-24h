package com.devcamp.shop24h.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.shop24h.model.CEmployee;
import com.devcamp.shop24h.model.repository.IEmployeeRepository;


@CrossOrigin
@Controller
public class EmployeeController {
	@Autowired
	IEmployeeRepository employeeRepository;
	
	@PostMapping("/employee")
	public ResponseEntity<Object> createEmployee(@RequestBody CEmployee employee) {
		try {
			CEmployee createEmployee = new CEmployee();
			createEmployee.setLastName(employee.getLastName());
			createEmployee.setFirstName(employee.getFirstName());
			createEmployee.setIntroduct(employee.getIntroduct());
			createEmployee.setEmail(employee.getEmail());
			createEmployee.setOfficeCode(employee.getOfficeCode());
			createEmployee.setReportTo(employee.getReportTo());
			createEmployee.setJobTitle(employee.getJobTitle());
			
			CEmployee saveEmployee = employeeRepository.save(createEmployee);
			return new ResponseEntity<>(saveEmployee, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Employee: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/employee/{id}")
	public ResponseEntity<Object> updateEmployee(@PathVariable Long id, @RequestBody CEmployee employee) {
		try {
			Optional<CEmployee> employeeData = employeeRepository.findById(id);
			if (employeeData.isPresent()) {
				CEmployee updateEmployee = employeeData.get();
				updateEmployee.setLastName(employee.getLastName());
				updateEmployee.setFirstName(employee.getFirstName());
				updateEmployee.setIntroduct(employee.getIntroduct());
				updateEmployee.setEmail(employee.getEmail());
				updateEmployee.setOfficeCode(employee.getOfficeCode());
				updateEmployee.setReportTo(employee.getReportTo());
				updateEmployee.setJobTitle(employee.getJobTitle());
				
				CEmployee saveEmployee = employeeRepository.save(updateEmployee);
				return new ResponseEntity<>(saveEmployee, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Update specified Employee: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/employee/{id}")
	public ResponseEntity<Object> deleteEmployee(@PathVariable Long id) {
		try {
			Optional<CEmployee> optional = employeeRepository.findById(id);
			if (optional.isPresent()) {
				employeeRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			} 
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/employee")
	public ResponseEntity<Object> deleteAllEmployee() {
		try {
			employeeRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
