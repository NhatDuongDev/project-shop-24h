package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.shop24h.model.CProductLine;
import com.devcamp.shop24h.model.repository.IProductLineRepository;

@CrossOrigin
@Controller
public class ProductLineController {
	@Autowired
	IProductLineRepository productLineRepository;
	
	@GetMapping("/productline/{id}")
	public ResponseEntity<CProductLine> getProductLineById(@PathVariable Long id) {
		try {
			Optional<CProductLine> productLine = productLineRepository.findById(id);
			if (productLine.isPresent()) {
				return new ResponseEntity<>(productLine.get(), HttpStatus.OK);
			} else
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/productline")
	public ResponseEntity<List<CProductLine>> getAllProductLine() {
		try {
			List<CProductLine> pProductLine = new ArrayList<>();
			productLineRepository.findAll().forEach(pProductLine::add);
			return new ResponseEntity<>(pProductLine, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/productline")
	public ResponseEntity<Object> createProductLine(@RequestBody CProductLine productLine) {
		try {
			CProductLine createProductLine = new CProductLine();
			createProductLine.setProductLine(productLine.getProductLine());
			createProductLine.setDescription(productLine.getDescription());
			
			CProductLine saveProductLine = productLineRepository.save(createProductLine);
			return new ResponseEntity<>(saveProductLine, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProductLine: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/productline/{id}")
	public ResponseEntity<Object> updateProductLine(@PathVariable Long id, @RequestBody CProductLine productLine) {
		try {
			Optional<CProductLine> productLineData = productLineRepository.findById(id);
			if (productLineData.isPresent()) {
				CProductLine updateProductLine = productLineData.get();
				updateProductLine.setProductLine(productLine.getProductLine());
				updateProductLine.setDescription(productLine.getDescription());
				
				CProductLine saveProductLine = productLineRepository.save(updateProductLine);
				return new ResponseEntity<>(saveProductLine, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Update specified productLine: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/productline/{id}")
	public ResponseEntity<Object> deleteProductLine(@PathVariable Long id) {
		try {
			Optional<CProductLine> optional = productLineRepository.findById(id);
			if (optional.isPresent()) {
				productLineRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			} 
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/productline")
	public ResponseEntity<Object> deleteAllProductLine() {
		try {
			productLineRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
