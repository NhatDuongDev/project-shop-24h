package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.shop24h.model.repository.IProductLineRepository;
import com.devcamp.shop24h.model.repository.IProductRepository;
import com.devcamp.shop24h.model.CProduct;
import com.devcamp.shop24h.model.CProductLine;

@CrossOrigin
@Controller
public class ProductController {
	@Autowired
	IProductRepository productRepository;
	@Autowired
	IProductLineRepository productLineRepository;
	
	// hàm gọi sản phẩm lấy về theo trang
	@GetMapping("/productPage")
	public ResponseEntity<List<CProduct>> getProductByPage(@RequestParam(name = "page", required = false) Integer page) {
		try {
			if (page == null) {
				List<CProduct> pProduct = productRepository.findAll();
				return new ResponseEntity<>(pProduct, HttpStatus.OK);
			} else {
				List<CProduct> pProduct = productRepository.getProductByPage(PageRequest.of(page - 1, 6));
				return new ResponseEntity<>(pProduct, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	// hàm gọi lấy sản phẩm theo trang của product line
		@GetMapping("/productline/{id}/productpage")
		public ResponseEntity<List<CProduct>> getPageProductByProductLine(@PathVariable int id, @RequestParam(name = "page", required = false) Integer page) {
			try {
				if (page == null) {
					List<CProduct> pProduct = productRepository.findByProductLineId(id);
					return new ResponseEntity<>(pProduct, HttpStatus.OK);
				} else {
					List<CProduct> pProduct = productRepository.getPageProductByProductLine(id, PageRequest.of(page - 1, 6));
					return new ResponseEntity<>(pProduct, HttpStatus.OK);
				}
				
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	
	@GetMapping("/product")
	public ResponseEntity<List<CProduct>> getAllProduct() {
		try {
			List<CProduct> pProduct = new ArrayList<CProduct>();
			productRepository.findAll().forEach(pProduct::add);
			return new ResponseEntity<>(pProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/productline/{id}/product")
	public ResponseEntity<List<CProduct>> getProductByProductLineId(@PathVariable Long id) {
		try {
			List<CProduct> pProduct = productRepository.findByProductLineId(id);
			return new ResponseEntity<>(pProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/product/{id}")
	public ResponseEntity<CProduct> getProductById(@PathVariable Long id) {
		try {
			Optional<CProduct> product = productRepository.findById(id);
			if (product.isPresent()) {
				return new ResponseEntity<>(product.get(), HttpStatus.OK);
			} else
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/productline/{id}/product")
	public ResponseEntity<Object> createProduct(@PathVariable Long id, @RequestBody CProduct product) {
		try {
			Optional<CProductLine> productLineData = productLineRepository.findById(id);
			if (productLineData.isPresent()) {
				CProduct createProduct = new CProduct();
				createProduct.setProductCode(product.getProductCode());
				createProduct.setProductName(product.getProductName());
				createProduct.setProductDescription(product.getProductDescription());
				createProduct.setProductImage(product.getProductImage());
				createProduct.setProductVendor(product.getProductVendor());
				createProduct.setQuantityInStock(product.getQuantityInStock());
				createProduct.setBuyPrice(product.getBuyPrice());
				createProduct.setProductWeight(product.getProductWeight());
				createProduct.setSellPrice(product.getSellPrice());
				createProduct.setProductLine(productLineData.get());
				
				CProduct saveProduct = productRepository.save(createProduct);
				return new ResponseEntity<>(saveProduct, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified product: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/product/{id}")
	public ResponseEntity<Object> updateProduct(@PathVariable Long id, @RequestBody CProduct product) {
		try {
			Optional<CProduct> productData = productRepository.findById(id);
			if (productData.isPresent()) {
				CProduct updateProduct = productData.get();
				updateProduct.setProductCode(product.getProductCode());
				updateProduct.setProductName(product.getProductName());
				updateProduct.setProductDescription(product.getProductDescription());
				updateProduct.setProductImage(product.getProductImage());
				updateProduct.setProductVendor(product.getProductVendor());
				updateProduct.setQuantityInStock(product.getQuantityInStock());
				updateProduct.setBuyPrice(product.getBuyPrice());
				updateProduct.setProductWeight(product.getProductWeight());
				updateProduct.setSellPrice(product.getSellPrice());
				
				CProduct saveProduct = productRepository.save(updateProduct);
				return new ResponseEntity<>(saveProduct, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Update specified product: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/product/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable Long id) {
		try {
			productRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/product")
	public ResponseEntity<Object> deleteAllProduct() {
		try {
			productRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
