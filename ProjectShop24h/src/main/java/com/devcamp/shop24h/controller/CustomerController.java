package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.shop24h.model.CCustomer;
import com.devcamp.shop24h.model.COrder;
import com.devcamp.shop24h.model.CPayment;
import com.devcamp.shop24h.model.CProduct;
import com.devcamp.shop24h.model.repository.ICustomerRepository;
import com.devcamp.shop24h.model.repository.IOrderDetailRepository;
import com.devcamp.shop24h.model.repository.IOrderRepository;
import com.devcamp.shop24h.model.repository.IPaymentRepository;
import com.devcamp.shop24h.model.repository.IProductRepository;



@CrossOrigin
@Controller
public class CustomerController {
	@Autowired
	ICustomerRepository customerRepository;
	
	@Autowired
	IProductRepository productRepository;
	
	@Autowired
	IOrderRepository orderRepository;
	
	@Autowired
	IOrderDetailRepository orderDetailRepository;
	
	@Autowired
	IPaymentRepository paymentRepository;
	
	
	@GetMapping("custometByPhoneNumber/{phone}")
	public ResponseEntity<Object> getCustomerByPhone(@PathVariable String phone) {
		try {
			Optional<CCustomer> getCustomer = customerRepository.findByPhoneNumber(phone);
			if (getCustomer.isPresent()) {
				return new ResponseEntity<>(getCustomer.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customer/{id}")
	public ResponseEntity<Object> getCustomerById(@PathVariable Long id) {
		try {
			Optional<CCustomer> customerData = customerRepository.findById(id);
			return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customer")
	public ResponseEntity<List<CCustomer>> getAllCustomer() {
		try {
			List<CCustomer> pCustomer = new ArrayList<>();
			customerRepository.findAll().forEach(pCustomer::add);
			return new ResponseEntity<>(pCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customerSelect")
	public ResponseEntity<List<Object>> getCustomerSelect() {
		try {
			List<Object> pCustomer = customerRepository.getCustomerSelect();
			return new ResponseEntity<>(pCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@PostMapping("/customerorder")
	public ResponseEntity<Object> createCustomerOrder(@RequestBody CCustomer customer) {
		try {
			
			Optional<CCustomer> customertData = customerRepository.findByPhoneNumber(customer.getPhoneNumber());
			if (customertData.isPresent()) {
				CCustomer getCustomer = customertData.get();
				COrder order = customer.getOrders().get(0);
				
				order.setComments(order.getComments());
				order.setOrderDate(new Date());
				order.setRequiredDate(new Date());
				order.setStatus("open");
				order.setCustomer(getCustomer);
				
				for (int i = 0; i < order.getOrderDetail().size(); i++) {
					long productId = order.getOrderDetail().get(i).getProducId();
					CProduct product = productRepository.findById(productId).get();
					order.getOrderDetail().get(i).setProduct(product);
					order.getOrderDetail().get(i).setOrder(order);
				}
				
				COrder saveOrder = orderRepository.save(order);
				
				CPayment payment = customer.getPayment().get(0);
				
				payment.setAmmount(payment.getAmmount());
				payment.setCheckNumber(payment.getCheckNumber());
				payment.setPaymentDate(payment.getPaymentDate());
				payment.setCustomer(getCustomer);
				
				paymentRepository.save(payment);
				
				return new ResponseEntity<>(saveOrder, HttpStatus.CREATED);
			} else {
				CCustomer createCustomer = customerRepository.save(customer);
				
				COrder order = createCustomer.getOrders().get(0);
				order.setStatus("open");
				order.setOrderDate(new Date());
				order.setRequiredDate(new Date());
				order.setCustomer(createCustomer);
				
				for (int i = 0; i < order.getOrderDetail().size(); i++) {
					long productId = order.getOrderDetail().get(i).getProducId();
					CProduct product = productRepository.findById(productId).get();
					order.getOrderDetail().get(i).setProduct(product);
					order.getOrderDetail().get(i).setOrder(order);
				}
				
				orderRepository.save(order);
				return new ResponseEntity<>(createCustomer, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/customer")
	public ResponseEntity<Object> createCustomer(@RequestBody CCustomer customer) {
		try {
			CCustomer createCustomer = new CCustomer();
			createCustomer.setLastname(customer.getLastname());
			createCustomer.setFirstname(customer.getFirstname());
			createCustomer.setPhoneNumber(customer.getPhoneNumber());
			createCustomer.setAddress(customer.getAddress());
			createCustomer.setCity(customer.getCity());
			createCustomer.setState(customer.getState());
			createCustomer.setPostalCode(customer.getPostalCode());
			createCustomer.setCountry(customer.getCountry());
			createCustomer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
			createCustomer.setCreditLimit(customer.getCreditLimit());
			
			CCustomer saveCustomer = customerRepository.save(createCustomer);
			return new ResponseEntity<>(saveCustomer, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Customer: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/customer/{id}")
	public ResponseEntity<Object> updateCustomer(@PathVariable Long id, @RequestBody CCustomer customer) {
		try {
			Optional<CCustomer> customerData = customerRepository.findById(id);
			if (customerData.isPresent()) {
				CCustomer updateCustomer = customerData.get();
				updateCustomer.setLastname(customer.getLastname());
				updateCustomer.setFirstname(customer.getFirstname());
				updateCustomer.setPhoneNumber(customer.getPhoneNumber());
				updateCustomer.setAddress(customer.getAddress());
				updateCustomer.setCity(customer.getCity());
				updateCustomer.setCountry(customer.getCountry());
				
				CCustomer saveCustomer = customerRepository.save(updateCustomer);
				return new ResponseEntity<>(saveCustomer, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Update specified productLine: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/customer/{id}")
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
		try {
			Optional<CCustomer> optional = customerRepository.findById(id);
			if (optional.isPresent()) {
				customerRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/customer")
	public ResponseEntity<CCustomer> deleteAllCustomer() {
		try {
			customerRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
