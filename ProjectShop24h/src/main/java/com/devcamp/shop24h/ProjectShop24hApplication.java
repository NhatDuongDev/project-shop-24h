package com.devcamp.shop24h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectShop24hApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectShop24hApplication.class, args);
	}

}
